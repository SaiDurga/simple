"""Implementation of binary search in linkedlist."""


class Node:
    """Node implementation for doubly-linked list."""

    def __init__(self, v=None, next=None, prev=None):
        """Construct a node."""
        self.value = v
        self.next = next
        self.prev = prev


class DoublyLinkedList:
    """Implementation of doubly-linked list."""

    def __init__(self, head=None, tail=None, length=0):
        """Construct a doubly-linked list."""
        self.head = head
        self.tail = tail
        self.length = length

    def is_empty(self):
        """Return True if the list is empty; False otherwise."""
        return self.length == 0

    def append(self, v):
        """Append given value to the linked list."""
        new_cell = Node(v, None, self.tail)
        if self.is_empty():
            self.head = new_cell
            self.tail = new_cell
            self.length = 1
            return self
        self.tail.next = new_cell
        self.tail = new_cell
        self.length = self.length + 1
        return self

    def seek(self, i):
        """Return value at index i from the end of the list."""
        n = self.head
        if i < 0 or i > self.length:
            return -1
        while i > 0:
            i = i-1
            n = n.next
        return n.value

    def binary_search(self, n):
        """Implement binary search in linkedlist."""
        i1 = 0
        i2 = self.length-1
        if self.length != 0:
            while(i1 <= i2):
                m = (i1+i2)//2
                if self.seek(m) == n:
                    return True
                elif self.seek(m) > n:
                    i2 = m-1
                else:
                    i1 = m+1
            return False
        return False


# %%


class TestDoublyLinkedList(object):
    """Test Binary search."""

    def test_binary_search(self):
        """Test binary search functionality."""
        la = DoublyLinkedList()
        la.append(1)
        la.append(20)
        la.append(33)
        la.append(47)
        la.append(55)
        assert la.binary_search(33) is True
        assert la.binary_search(1) is True
        assert la.binary_search(20) is True
        assert la.binary_search(47) is True
        assert la.binary_search(55) is True
        assert la.binary_search(0) is False
        assert la.binary_search(-1) is False
        assert la.binary_search(56) is False
        assert la.binary_search(330) is False
