"""Chapter-10 in introduction to algorithms."""


# %%


class Stack:
    """Implement stack."""

    def __init__(self, size):
        """Stack."""
        self.size = size
        self.list = [None] * size
        self.top = 0

    def push(self, ele):
        """Push function."""
        if self.top < self.size:
            self.list[self.top] = ele
            self.top = self.top + 1
        else:
            print("overflow")

    def pop(self):
        """Pop function."""
        if self.stack_empty() is True:
            print("underflow")
        else:
            x = self.list[self.top]
            self.top = self.top - 1
            return x

    def stack_empty(self):
        """Check whether the stack is empty or not."""
        if self.top == 0:
            return True
        else:
            return False


# %%


class Queue():
    """Implement Queue."""

    def __init__(self, size):
        """Queue."""
        self.size = size
        self.list = [None] * size
        self.head = 0
        self.tail = 0

    def enqueue(self, ele):
        """Implement enque."""
        if self.head == self.tail and self.list[self.head+1] is not None:
            print("overflow")
        else:
            self.list[self.tail] = ele
            if self.tail == self.size-1:
                self.tail = 0
            else:
                self.tail = self.tail+1

    def dequeue(self):
        """Implement deque."""
        if self.head == self.tail and self.list[self.head+1] is None:
            print("underflow")
        else:
            x = self.list[self.head]
            if self.head == self.size:
                self.head = 0
            else:
                self.head = self.head+1
            return x


# %%


class two_stacks_one_array():
    """Implement two stacks in one array."""

    def __init__(self, size):
        """Two stacks in one array."""
        self.size = size
        self.list = [None]*size
        self.top1 = -1
        self.top2 = size

    def push_stack1(self, ele):
        """Push elements into first stack."""
        if self.top1+1 == self.top2:
            print("overflow")
        else:
            self.list[self.top1+1] = ele
            self.top1 = self.top1+1

    def top_1(self):
        """Top element for stack1."""
        if self.top1 >= 0:
            return self.list[self.top1]

    def pop_stack1(self):
        """Pop elements from stack1."""
        if self.top1 == -1:
            print("stack1-underflow")
        else:
            x = self.list[self.top1]
            self.top1 = self.top1 - 1
            return x

    def push_stack2(self, ele):
        """Push elements into second stack."""
        if self.top1+1 == self.top2:
            print("overflow")
        else:
            self.list[self.top2-1] = ele
            self.top2 = self.top2-1

    def top_2(self):
        """Top element for stack1."""
        if self.top2 < 8:
            return self.list[self.top2]

    def pop_stack2(self):
        """Pop elements from stack2."""
        if self.top2 == self.size:
            print("stack2-underflow")
        else:
            x = self.list[self.top2]
            self.top2 = self.top2 + 1
            return x


# %%


class Deque():
    """Implement deque."""

    def __init__(self, size):
        """Implement double ended queue."""
        self.size = size
        self.list = [None] * size
        self.head = 0
        self.tail = 0

    def enqueue_tail(self, ele):
        """Implement enque at tail."""
        if self.head == self.tail and self.list[self.head+1] is not None:
            print("overflow")
        else:
            self.list[self.tail] = ele
            if self.tail == self.size-1:
                self.tail = 0
            else:
                self.tail = self.tail+1

    def dequeue_head(self):
        """Implement deque at head."""
        if self.head == self.tail and self.list[self.head+1] is None:
            print("underflow")
        else:
            x = self.list[self.head]
            if self.head == self.size:
                self.head = 0
            else:
                self.head = self.head+1
            return x

    def dequeue_tail(self):
        """Implement deque at tail."""
        if self.head == self.tail and self.list[self.head+1] is None:
            print("underflow")
        else:
            x = self.list[self.tail-1]
            if self.tail == 0:
                self.tail = self.size-1
            else:
                self.tail = self.tail-1
            return x

    def enqueue_head(self, ele):
        """Implement enque at head."""
        if self.head == self.tail and self.list[self.head-1] is not None:
            print("overflow")
        else:
            self.list[self.head-1] = ele
            if self.head == 0:
                self.head = self.size
            else:
                self.head = self.head - 1

# %%


class Queue_using_stacks():
    """Implement a queue using two stacks."""

    def __init__(self, size):
        """Create objects for stack."""
        self.stack1 = Stack(size)
        self.stack2 = Stack(size)

    def enQueue(self, ele):
        """ENQUEUE using stacks."""
        self.stack1.push(ele)

    def dequeue(self):
        """DEQUEUE using stacks."""
        for i in range(self.stack1.top):
            x = self.stack1.pop()
            self.stack2.push(x)
        y = self.stack2.pop()
        for i in range(self.stack2.top):
            x = self.stack2.pop()
            self.stack1.push(x)
        return y


# %%


class Stack_using_queues():
    """Implement a queue using two stacks."""

    def __init__(self, size):
        """Create objects for queues."""
        self.size = size
        self.queue1 = Queue(size)
        self.queue2 = Queue(size)

    def push(self, ele):
        """Push operation using queues."""
        self.queue1.enqueue(ele)

    def pop(self):
        """Pop operation using queues."""
        for i in range(self.queue1.tail-1):
            x = self.queue1.dequeue()
            self.queue2.enqueue(x)
        y = self.queue1.list[self.queue1.tail - 1]
        self.queue1 = Queue(self.size)
        for i in range(self.queue2.tail):
            x = self.queue2.dequeue()
            self.queue1.enqueue(x)
        return y
