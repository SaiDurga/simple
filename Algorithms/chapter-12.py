"""Chapter-12 in introduction to algorithms."""


# %%


class Node:
    """Implement binary search trees."""

    def __init__(self, key):
        """Node for the binary search tree."""
        self.key = key
        self.left = None
        self.right = None


def inorder_tree_walk(x):
    """Implement inorder tree walk."""
    if x:
        inorder_tree_walk(x.left)
        print(x.key)
        inorder_tree_walk(x.right)


# %%
n = Node(4)
n.left = Node(2)
n.right = Node(6)
n.left.left = Node(1)
n.left.right = Node(3)
n.right.left = Node(5)
n.right.right = Node(7)
inorder_tree_walk(n)

# %%


def tree_search(x, k):
    """Implemnt search operation."""
    if not x or k == x.key:
        return x
    if k < x.key:
        return tree_search(x.left, k)
    else:
        return tree_search(x.right, k)


tree_search(n, 1)
n.left.left == tree_search(n, 1)
n.left.right == tree_search(n, 3)

# %%


def iterative_tree_search(x, k):
    """Implement iterative search operation."""
    while x and k != x.key:
        if k < x.key:
            x = x.left
        else:
            x = x.right
    return x


iterative_tree_search(n, 1)
n.left.left == iterative_tree_search(n, 1)
n.left.right == iterative_tree_search(n, 3)

# %%


def tree_minimum(x):
    """Implement minimum operation."""
    while x.left:
        x = x.left
    return x


tree_minimum(n).key

# %%


def recursive_tree_minimum(x):
    """Implement tree minimum operation using recursion."""
    if x.left:
        return recursive_tree_minimum(x.left)
    else:
        return x


recursive_tree_minimum(n).key

# %%


def tree_maximum(x):
    """Implement minimum operation."""
    while x.right:
        x = x.right
    return x


tree_maximum(n).key

# %%


def recursive_tree_maximum(x):
    """Implement tree maximum operation using recursion."""
    if x.right:
        return recursive_tree_maximum(x.right)
    else:
        return x


recursive_tree_maximum(n).key

# %%


def tree_successor(x):
    """Implement tree successor."""
    if x.right:
        return tree_minimum(x.right)
    y = x.p
    while y and x == y.right:
        x = y
        y = y.p
    return y


tree_successor(n) == n.right.left
tree_successor(n) == n.right

# %%


def tree_predecessor(x):
    """Implement tree predecessor."""
    if x.left:
        return tree_maximum(x.left)
    y = x.p
    while y and x == y.left:
        x = y
        y = y.p
    return y


tree_predecessor(n) == n.left.right

# %%


def post_order_treewalk(x):
    """Implement post order tree walk."""
    if x:
        post_order_treewalk(x.left)
        post_order_treewalk(x.right)
        print(x.key)


post_order_treewalk(n)


# %%


def pre_order_treewalk(x):
    """Implement pre order tree walk."""
    if x:
        print(x.key)
        pre_order_treewalk(x.left)
        pre_order_treewalk(x.right)


pre_order_treewalk(n)
