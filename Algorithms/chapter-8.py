"""Practice chapter-8."""

import math

# %%


def counting_sort(A, k):
    """Implement counting sort."""
    C = [0 for i in range(k+1)]
    B = [0 for i in range(len(A))]
    for j in range(len(A)):
        C[A[j]] = C[A[j]]+1
    for i in range(1, k+1):
        C[i] = C[i] + C[i-1]
    for j in range(len(A)-1, -1, -1):
        b = C[A[j]]
        B[b-1] = A[j]
        C[A[j]] = C[A[j]] - 1
    return B


counting_sort([1, 0, 3, 2, 8, 4, 5, 6], 8)

# %%


def insertion_sort(lst):
    """Implement Insertion sort."""
    for j in range(1, len(lst)):
        key = lst[j]
        i = j-1
        while i >= 0 and lst[i] > key:
            lst[i+1] = lst[i]
            i = i-1
        lst[i+1] = key
    return lst


insertion_sort([0.79, 0.81, 0.76])


# %%


def bucket_sort(A):
    """Implement bucket sort."""
    B = []
    n = len(A)
    B = [[] for i in range(n)]
    for i in range(0, n):
        B[math.floor(n*A[i])].append(A[i])
    for i in range(0, n-1):
        insertion_sort(B[i])
    res = []
    for i in range(n):
        res = res+B[i]
    return res


bucket_sort([0.1, 0.23, 0.12, 0.43, 0.78, 0.76])
bucket_sort([.79, .13, .16, .64, .39, .20, .89, .53, .71, .42])

# %%
