"""Data Structures and Algorithms."""

import numpy as np
from collections import deque
import heapq
from collections import defaultdict
from collections import OrderedDict
from collections import Counter
from operator import itemgetter
from itertools import compress
from collections import namedtuple
from collections import ChainMap

# %%
# Unpacking a Sequence into Separate Variables
p = (4, 5)
x, y = p
x
y

data = ['ACME', 50, 91.1, (2012, 12, 21)]
name, shares, price, date = data
name
shares
price
date

name, shares, price, (year, mon, day) = data
year
mon
day

# x, y, z = p

# %%
s = 'Hello'
a, b, c, d, e = s
e

# %%
_, shares, price, _ = data
shares
price
_
_

# %%
# 1.2. Unpacking Elements from Iterables of Arbitrary
record = ('Dave', 'dave@example.com', '773-555-1212', '847-555-1212')
name, email, *phone_numbers = record
name
email
phone_numbers

# %%
*trailing_qtrs, current_qtr = sales_record
trailing_avg = sum(trailing_qtrs) / len(trailing_qtrs)
# print(avg_comparison(trailing_avg, current_qtr))

*trailing, current = [10, 8, 7, 1, 9, 5, 10, 3]
np.mean(trailing)
current

# %%
records = [
            ('foo', 1, 2),
            ('bar', 'hello'),
            ('foo', 3, 4),
        ]


def do_foo(x, y):
    """Write function for foo."""
    print('foo', x, y)


def do_bar(s):
    """Write function for bar."""
    print('bar', s)


for tag, *args in records:
    if tag == 'foo':
        do_foo(*args)
    elif tag == 'bar':
        do_bar(*args)

items = [1, 10, 7, 4, 5, 9]
head, *tail = items
head
tail


def sum(items):
    """Sum of items."""
    head, *tail = items
    return head + sum(tail) if tail else head


sum(items)

# %%
# Keeping the Last N Items
q = deque(maxlen=3)
q.append(1)
q.append(2)
q.append(3)
q
q.append(4)
q
q.append(5)
q

# %%
# Finding the Largest or Smallest N Items
nums = [1, 8, 2, 23, 7, -4, 18, 23, 42, 37, 2]
print(heapq.nlargest(3, nums))
print(heapq.nlargest(5, nums))
print(heapq.nsmallest(3, nums))
print(heapq.nsmallest(5, nums))

# %%
portfolio = [
    {'name': 'IBM', 'shares': 100, 'price': 91.1},
    {'name': 'AAPL', 'shares': 50, 'price': 543.22},
    {'name': 'FB', 'shares': 200, 'price': 21.09},
    {'name': 'HPQ', 'shares': 35, 'price': 31.75},
    {'name': 'YHOO', 'shares': 45, 'price': 16.35},
    {'name': 'ACME', 'shares': 75, 'price': 115.65}
]
portfolio

cheap = heapq.nsmallest(3, portfolio, key=lambda s: s['price'])
expensive = heapq.nlargest(3, portfolio, key=lambda s: s['price'])
cheap
expensive

# %%
nums = [1, 8, 2, 23, 7, -4, 18, 23, 42, 37, 2]
nums
heap = list(nums)
heap
heapq.heapify(heap)
heap
heapq.heappop(heap)
heapq.heappop(heap)
heapq.heappop(heap)
heap

# %%


class Item:
    """Write a class for item."""

    def __init__(self, name):
        """Init in item."""
        self.name = name

    def __repr__(self):
        """Represent."""
        return 'Item({!r})'.format(self.name)


a = Item('foo')
b = Item('bar')
a < b

# %%
a = (1, Item('foo'))
b = (2, Item('bar'))
a < b

# %%
# Mapping Keys to Multiple Values in a Dictionary
pairs = ([1, 2], [1, 3], [2, 4], [3, 2], [2, 8])
d = {}
for key, value in pairs:
    if key not in d:
        d[key] = []
    d[key].append(value)
d

# %%
d = defaultdict(list)
for key, value in pairs:
    d[key].append(value)
d

# %%
# Keeping Dictionaries in Order
d = OrderedDict()
d['foo'] = 1
d['bar'] = 2
d['spam'] = 3
d['grok'] = 4

for key in d:
    print(key, d[key])

# %%
# Calculating with Dictionaries
prices = {'ACME': 45.23, 'AAPL': 612.78, 'IBM': 205.55, 'HPQ': 37.20,
          'FB': 10.75}
prices
min_price = min(zip(prices.values(), prices.keys()))
min_price
max_price = max(zip(prices.values(), prices.keys()))
max_price
prices_sorted = sorted(zip(prices.values(), prices.keys()))
prices_sorted

# %%
prices_and_names = zip(prices.values(), prices.keys())
print(min(prices_and_names))
print(max(prices_and_names))

# %%
min(prices, key=lambda k: prices[k])
min_value = prices[min(prices, key=lambda k: prices[k])]
min_value

# %%
# Finding Commonalities in Two Dictionaries
a = {'x': 1, 'y': 2, 'z': 3}
a
b = {'w': 1, 'x': 11, 'y': 2}
b
a.keys() & b.keys()
a.keys() - b.keys()
a.items() & b.items()

# %%
# Removing Duplicates from a Sequence while Maintaining Order


def dedupe(items):
    """Remove duplicates."""
    seen = set()
    for item in items:
        if item not in seen:
            yield item
        seen.add(item)


a = [1, 5, 2, 1, 9, 1, 5, 10]
list(dedupe(a))

# %%


def dedupe1(items):
    """Remove duplicates."""
    seen = set()
    for item in items:
        val = item if key is None else key(item)
        if val not in seen:
            yield item
        seen.add(val)


a = [{'x': 1, 'y': 2}, {'x': 1, 'y': 3}, {'x': 1, 'y': 2}, {'x': 2, 'y': 4}]
list(dedupe1(a, key=lambda d: (d['x'], d['y'])))
list(dedupe1(a, key=lambda d: d['x']))

# %%
# Naming a Slice
items = [0, 1, 2, 3, 4, 5, 6]
a = slice(2, 4)
items[2:4]
items[a]
items[a] = [10, 11]
items
del items[a]
items

# %%
a = slice(10, 50, 2)
a.start
a.stop
a.step

# %%
# Determining the Most Frequently Occurring Items in a Sequence
words = ['look', 'into', 'my', 'eyes', 'look', 'into', 'my', 'eyes', 'the',
         'eyes', 'the', 'eyes', 'the', 'eyes', 'not', 'around', 'the',
         'eyes', "don't", 'look', 'around', 'the', 'eyes', 'look', 'into',
         'my', 'eyes', "you're", 'under']
words
word_counts = Counter(words)
top_three = word_counts.most_common(3)
print(top_three)

# %%
word_counts['not']
word_counts['eyes']

# %%
morewords = ['why', 'are', 'you', 'not', 'looking', 'in', 'my', 'eyes']
word_counts.update(morewords)
word_counts['eyes']
a = Counter(words)
b = Counter(morewords)
a
b
c = a + b
c
d = a - b
d

# %%
# Sorting a List of Dictionaries by a Common Key
rows = [
    {'fname': 'Brian', 'lname': 'Jones', 'uid': 1003},
    {'fname': 'David', 'lname': 'Beazley', 'uid': 1002},
    {'fname': 'John', 'lname': 'Cleese', 'uid': 1001},
    {'fname': 'Big', 'lname': 'Jones', 'uid': 1004}
]
rows
rows_by_fname = sorted(rows, key=itemgetter('fname'))
rows_by_fname
rows_by_uid = sorted(rows, key=itemgetter('uid'))
rows_by_uid
rows_by_lfname = sorted(rows, key=itemgetter('lname', 'fname'))
rows_by_lfname
min(rows, key=itemgetter('uid'))
max(rows, key=itemgetter('uid'))

# %%
# Filtering Sequence Elements
mylist = [1, 4, -5, 10, -7, 2, 3, -1]
[n for n in mylist if n > 0]
[n for n in mylist if n < 0]
# use generators
pos = (n for n in mylist if n > 0)
pos
for x in pos:
    print(x)

values = ['1', '2', '-3', '-', '4', 'N/A', '5']


def is_int(val):
    """Find integers."""
    try:
        x = int(val)
        return True
    except ValueError:
        return False


ivals = list(filter(is_int, values))
print(ivals)

clip_neg = [n if n > 0 else 0 for n in mylist]
clip_neg
clip_pos = [n if n < 0 else 0 for n in mylist]
clip_pos


# %%
addresses = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
counts = [0, 3, 10, 4, 1, 7, 6, 1]

more5 = [n > 5 for n in counts]
more5
list(compress(addresses, more5))

# %%
# Extracting a Subset of a Dictionary
prices
p1 = {key: value for key, value in prices.items() if value > 200}
p1

tech_names = {'AAPL', 'IBM', 'HPQ', 'MSFT'}
p2 = {key: value for key, value in prices.items() if key in tech_names}
p2

# %%
# Mapping Names to Sequence Elements
Subscriber = namedtuple('Subscriber', ['addr', 'joined'])
sub = Subscriber('jonesy@example.com', '2012-10-19')
sub
sub.addr
sub.joined
len(sub)
addr, joined = sub
addr
joined

# %%
Stock = namedtuple('Stock', ['name', 'shares', 'price'])
s = Stock('ACME', 100, 123.45)
s
s.shares = 75
s = s._replace(shares=75)

# %%
# Transforming and Reducing Data at the Same Time
nums = [1, 2, 3, 4, 5]
s = sum((x * x for x in nums))
s
sum(x * x for x in nums)

# %%
# Combining Multiple Mappings into a Single Mapping
a = {'x': 1, 'z': 3}
b = {'y': 2, 'z': 4}
c = ChainMap(a, b)
c
print(c['x'])
print(c['y'])
print(c['z'])
print(c['z'])
len(c)
list(c.keys())
list(c.values())
c['z'] = 10
c['w'] = 40
print(c['z'])
print(c['w'])
del c['x']
list(c.keys())
del c['y']

# %%
values = ChainMap()
values['x'] = 1
values = values.new_child()
values['x'] = 2
values = values.new_child()
values['x'] = 3
values
values['x']
values = values.parents
values['x']
values = values.parents
values['x']
values
