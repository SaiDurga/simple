"""Iterators and generators."""

import itertools
from itertools import islice
from itertools import permutations
from itertools import combinations
from itertools import combinations_with_replacement
from itertools import zip_longest
from itertools import chain
from collections import Iterable
import heapq

# Manually consuming an iterator
items = [1, 2, 3]
it = iter(items)
next(it)
next(it)
next(it)
next(it)

# %%
# Creating New Iteration Patterns with Generators


def frange(start, stop, increment):
    """Implement new kind of Iteration."""
    x = start
    while x < stop:
        yield x
        x += increment


for n in frange(0, 4, 0.5):
    print(n)

list(frange(0, 1, 0.125))

# %%


def countdown(n):
    """Yield statement."""
    print('Starting to count from', n)
    while n > 0:
        yield n
        n -= 1
    print('Done!')


c = countdown(3)
c

next(c)
next(c)
next(c)
next(c)

# %%
# Iterating in Reverse
a = [1, 2, 3, 4]
for x in reversed(a):
    print(x)

# %%
# Taking a Slice of an Iterator


def count(n):
    """Count function."""
    while True:
        yield n
        n += 1


c = count(0)
c[10:20]

# %%
for x in itertools.islice(c, 10, 30):
    print(x)

# %%
# Skipping the First Part of an Iterable
items = ['a', 'b', 'c', 1, 4, 10, 15]
for x in islice(items, 3, None):
    print(x)

# %%
for x in islice(items, 4, None):
    print(x)

# %%
for x in islice(items, 2, 4):
    print(x)

# %%
# Iterating Over All Possible Combinations or Permutations
# posssible combinations
items = ['a', 'b', 'c']
for p in permutations(items):
    print(p)

# %%
# permutations of smaller length
for p in permutations(items, 2):
    print(p)

# %%
# combinations
for c in combinations(items, 3):
    print(c)

# %%
for c in combinations(items, 2):
    print(c)

# %%
for c in combinations(items, 1):
    print(c)

# %%
for c in combinations_with_replacement(items, 3):
    print(c)

# %%
for c in combinations_with_replacement(items, 2):
    print(c)

# %%
# Iterating Over the Index-Value Pairs of a Sequence
my_list = ['a', 'b', 'c']
for idx, val in enumerate(my_list):
    print(idx, val)

# %%
for idx, val in enumerate(my_list, 1):
    print(idx, val)

# %%
# Iterating Over Multiple Sequences Simultaneously
xpts = [1, 5, 4, 2, 10, 7]
ypts = [101, 78, 37, 15, 62, 99]
for x, y in zip(xpts, ypts):
    print(x, y)

# %%
a = [1, 2, 3]
b = ['w', 'x', 'y', 'z']
for i in zip(a, b):
    print(i)

# %%
for i in zip_longest(a, b):
    print(i)

# %%
for i in zip_longest(a, b, fillvalue=0):
    print(i)

# %%
headers = ['name', 'shares', 'price']
values = ['ACME', 100, 490.1]
s = dict(zip(headers, values))
for name, val in zip(headers, values):
    print(name, '=', val)

# %%
a = [1, 2, 3]
b = [10, 11, 12]
c = [20, 30, 40]
for i in zip(a, b, c):
    print(i)

# %%
zip(a, b)
list(zip(a, b))

# %%
# Iterating on Items in Separate Containers
a = [1, 2, 3, 4]
a
b = ['x', 'y', 'z']
b

for x in chain(a, b):
    print(x)

# %%
# Flattening a Nested Sequence


def flatten(items, ignore_types=(str, bytes)):
    """Flatten the nested lists."""
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, ignore_types):
            yield from flatten(x)
        else:
            yield x


items = [1, 2, [3, 4, [5, 6], 7], 8]

for x in flatten(items):
    print(x)

# %%
items = ['Dave', 'Paula', ['Thomas', 'Lewis']]
for x in flatten(items):
    print(x)

# %%
# Iterating in Sorted Order Over Merged Sorted Iterables
a = [1, 4, 7, 10]
b = [2, 5, 6, 11]
for c in heapq.merge(a, b):
    print(c)
