"""Metaprogramming."""

from functools import wraps
from inspect import signature
import logging

# %%
# Putting a Wrapper Around a Function


def decorator(func):
    """Write decorator that implements the given func."""
    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(* args, **kwargs)
        print(func.__name__, result)
    return wrapper


@decorator
def add(*args):
    """Add given numbers."""
    return sum(*args)


@decorator
def sub(x, y):
    """Subtract two numbers."""
    return x - y


# %%
add([2, 3, 5])
sub(3, 2)

# %%
# Preserving Function Metadata When Writing Decorators

add.__name__
add.__doc__
add.__annotations__

# %%


def decorator(func):
    """Write decorator that implements the given func."""
    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(* args, **kwargs)
        print(func.__name__, result)
        # return result
    return wrapper


@decorator
def add(*args):
    """Add given numbers."""
    return sum([*args])


@decorator
def sub(x, y):
    """Subtract two numbers."""
    return x - y


add.__name__
add.__doc__
add.__annotations__

add(2, 3)
add.__wrapped__(2, 3)
print(signature(add))

# %%
orig_add = add.__wrapped__
orig_add(3, 4)

# %%
# Unwrapping a Decorator


def decorator1(func):
    """Decorate 1."""
    @wraps(func)
    def wrapper(*args, **kwargs):
        print('Decorator 1')
        print(func(*args, **kwargs))
    return wrapper


def decorator2(func):
    """Decorate 2."""
    @wraps(func)
    def wrapper(*args, **kwargs):
        print('Decorator 2')
        return func(*args, **kwargs)
    return wrapper


@decorator1
@decorator2
def add1(x, y):
    """Add numbers."""
    return x + y


add1(2, 3)
add1.__wrapped__(2, 3)

# %%
# Defining a Decorator That Takes Arguments


def logged(level, name=None, message=None):
    """Add logging to a function."""
    def decorate(func):
        """Write decorator that implements the given func."""
        logname = name if name else func.__module__
        log = logging.getLogger(logname)
        logmsg = message if message else func.__name__

        @wraps(func)
        def wrapper(*args, **kwargs):
            log.log(level, logmsg)
            result = func(* args, **kwargs)
            print(func.__name__, result)
        return wrapper
    return decorate


@logged(logging.DEBUG)
def add(x, y):
    """Add given numbers."""
    return x + y


# %%


def argument(x, y, z):
    """Add arguments to decorator."""
    def decorator(func):
        """Write decorator that implements the given func."""
        @wraps(func)
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            print(func.__name__, result)
            print(x, y, z)
            # return result
        return wrapper
    return decorator


@decorator(x, y, z)
def add(a, b):
    """Add given numbers."""
    return a + b


# %%
# Defining Decorators As Part of a Class


class A:
    """Define Decorators as a part of class."""

    def decorator1(self, func):
        """Decorate 1."""
        @wraps(func)
        def wrapper(*args, **kwargs):
            print('Decorator 1')
            print(func(*args, **kwargs))
        return wrapper

    @classmethod
    def decorator2(cls, func):
        """Decorate 2."""
        @wraps(func)
        def wrapper(*args, **kwargs):
            print('Decorator 2')
            print(func(*args, **kwargs))
        return wrapper


a = A()


@a.decorator1
def add(x, y):
    """Add given numbers."""
    return x + y


@A.decorator2
def sub(x, y):
    """Subtract two numbers."""
    return x - y


add(2, 3)
sub(3, 2)

# %%
# Applying decoratots to class and static methods.


def decorator(func):
    """Write decorator that implements the given func."""
    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(* args, **kwargs)
        print(func.__name__, result)
    return wrapper


class Spam:
    """Apply decorators to class and static methods."""

    @decorator
    def instance_method(self, x, y):
        """Add given numbers."""
        return x + y

    @classmethod
    @decorator
    def class_method(self, x, y):
        """Add given numbers."""
        return x + y

    @staticmethod
    @decorator
    def static_method(x, y):
        """Add given numbers."""
        return x + y


s = Spam()
s.instance_method(3, 2)
Spam.class_method(3, 2)
Spam.static_method(3, 2)

# %%
# Writing Decorators That Add Arguments to Wrapped Functions


def optional_debug(func):
    """Write decorator to add arguments to wrappe functions."""
    @wraps(func)
    def wrapper(*args, debug=False, **kwargs):
        if debug:
            print('Calling', func.__name__)
        return func(*args, **kwargs)
    return wrapper


@optional_debug
def spam(a, b, c):
    """Spam."""
    print(a, b, c)


spam(1, 2, 3)
spam(1, 2, 3, debug=True)
