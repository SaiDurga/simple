"""Concurrency."""

import time
from threading import Thread, Event

# %%


def countdown(n):
    """Count down."""
    while n > 0:
        print('T-minus', n)
        n -= 1
        time.sleep(5)


t = Thread(target=countdown, args=(10,))

countdown(3)

t.start()

# %%

if t.is_alive():
    print('Still running')
else:
    print('Completed')

# %%


class CountdownTask:
    """Thread termination."""

    def __init__(self):
        """Run stage."""
        self._running = True

    def terminate(self):
        """Terminate thread."""
        self._running = False

    def run(self, n):
        """Thread."""
        while self._running and n > 0:
            print('T-minus', n)
            n -= 1
            time.sleep(5)


c = CountdownTask()
t = Thread(target=c.run, args=(10,))
t.start()
c.terminate()
t.join()

# %%
# Determining If a Thread Has Started


def countdown(n, started_evt):
    """Determine start of a thread."""
    print('countdown starting')
    started_evt.set()
    while n > 0:
        print('T-minus', n)
        n -= 1
        time.sleep(5)


started_evt = Event()

print('Launching countdown')
t = Thread(target=countdown, args=(10, started_evt))
t.start()
started_evt.wait()
print('countdown is running')
