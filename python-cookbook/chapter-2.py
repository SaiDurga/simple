"""Strings and Text."""

import re
from fnmatch import fnmatch, fnmatchcase
import string
import textwrap
import html
from html.parser import HTMLParser
import re

# %%
line = 'asdf fjdk; afed, fjek,asdf,     foo'
re.split(r'[;,\s]\s*', line)

line.split()

# %%
fields = re.split(r'(;|,|\s)\s*', line)
fields
values = fields[::2]
delimiters = fields[1::2] + ['']
values
delimiters
''.join(v+d for v, d in zip(values, delimiters))

# %%
re.split(r'(?:,|;|\s)\s*', line)

# %%
# Matching Text at the Start or End of a String
filename = 'spam.txt'
filename.endswith('.txt')
filename.startswith('file:')
url = 'http://www.python.org'
url.startswith('http:')

# %%
filenames = ['Makefile', 'foo.c', 'bar.py', 'spam.c', 'spam.h']
filenames
[name for name in filenames if name.endswith(('.c', '.h'))]
any(name.endswith('.py') for name in filenames)

# %%
re.match('http:|https:|ftp:', url)

# %%
# matching strings using shell wildcard patterns
fnmatch('foo.txt', '*.txt')
fnmatch('foo.txt', '?oo.txt')
fnmatch('Dat45.csv', 'Dat[0-9]*')
names = ['Dat1.csv', 'Dat2.csv', 'config.ini', 'foo.py']
[name for name in names if fnmatch(name, 'Dat*.csv')]
fnmatch('foo.txt', '*.TXT')
fnmatchcase('foo.txt', '*.TXT')

addresses = ['a vjd', 'b vjd', 'c hyd', 'd bzw']
[addr for addr in addresses if fnmatchcase(addr, '* vjd')]

# %%
# matching and searching for text patterns
text = 'yeah, but no, but yeah, but no, but yeah'
text == 'yeah'
text.startswith('yeah')
text.endswith('no')
text.find('no')

# %%
text1 = '11/27/2012'
text2 = 'Nov 27, 2012'
if re.match(r'\d+/\d+/\d+', text1):
    print('yes')
else:
    print('no')

if re.match(r'\d+/\d+/\d+', text2):
    print('yes')
else:
    print('no')

# %%
datepat = re.compile(r'\d+/\d+/\d+')
if datepat.match(text1):
    print('yes')
else:
    print('no')

if datepat.match(text2):
    print('yes')
else:
    print('no')

text = 'Today is 11/27/2012. PyCon starts 3/13/2013.'
datepat.findall(text)

# %%
datepat = re.compile(r'(\d+)/(\d+)/(\d+)')
m = datepat.match('11/27/2012')
m
m.group(0)
m.group(1)
m.group(2)
m.group(3)
m.groups()
month, day, year = m.groups()

# %%
text
datepat.findall(text)
for month, day, year in datepat.findall(text):
    print('{}-{}-{}'.format(year, month, day))

for m in datepat.finditer(text):
    print(m.groups())

# %%
# Searching and Replacing Text
text = 'yeah, but no, but yeah, but no, but yeah'
text
text.replace('yeah', 'yep')
re.sub('no', 'yes', text)

# %%
text = 'Today is 11/27/2012. PyCon starts 3/13/2013.'
re.sub(r'(\d+)/(\d+)/(\d+)', r'\3-\1-\2', text)
datepat = re.compile(r'(\d+)/(\d+)/(\d+)')
datepat.sub(r'\3-\1-\2', text)

# %%
# Searching and Replacing Case-Insensitive Text
text = 'UPPER PYTHON, lower python, Mixed Python'
re.findall('python', text, flags=re.IGNORECASE)
re.findall('python', text)
re.sub('python', 'snake', text, flags=re.IGNORECASE)


def matchcase(word):
    """Match case."""
    def replace(m):
        """Replace."""
        text = m.group()
        if text.isupper():
            return word.upper()
        elif text.islower():
            return word.lower()
        elif text[0].isupper():
            return word.capitalize()
        else:
            return word
    return replace


re.sub('python', matchcase('snake'), text, flags=re.IGNORECASE)

# %%
# Specifying a Regular Expression for the Shortest Match
str_pat = re.compile(r'\"(.*)\"')
text1 = 'Computer says "no."'
str_pat.findall(text1)
text2 = 'Computer says "no." Phone says "yes."'
str_pat.findall(text2)

# %%
str_pat = re.compile(r'\"(.*?)\"')
str_pat.findall(text2)

# %%
# Writing a Regular Expression for Multiline Patterns
comment = re.compile(r'/\*(.*?)\*/')
text1 = '/* this is a comment */'
text2 = '''/* this is a
              multiline comment */'''
comment.findall(text1)
comment.findall(text2)

# %%
comment = re.compile(r'/\*((?:.|\n)*?)\*/')
comment.findall(text2)

# %%
# Stripping Unwanted Characters from Strings
s = '   hello world \n'
s.strip()
s.lstrip()
s.rstrip()

# %%
t = '-------hello========'
t.lstrip('-')
t.strip('-=')

# %%
# Sanitizing and Cleaning Up Text
s = 'pýtĥöñ\fis\tawesome\r\n'
s
# %%
remap = {ord('\t'): ' ', ord('\f'): ' ', ord('\r'): None}
a = s.translate(remap)
a

# %%
# Aligning Text Strings
text = 'Hello World'
text.ljust(20)
text.rjust(20)
text.center(20)
text.rjust(20, '=')
text.center(20, '*')

# %%
format(text, '>20')
format(text, '<20')
format(text, '^20')
format(text, '=>20')
format(text, '*^20')
'{:>10s} {:>10s}'.format('Hello', 'World')
'{:>10s} {:>20s}'.format('Hello', 'World')

# %%
# Combining and concatenating strings
parts = ['Is', 'Chicago', 'Not', 'Chicago?']
''.join(parts)
' '.join(parts)
','.join(parts)
'|'.join(parts)

# %%
a = 'Is Chicago'
b = 'Not Chicago?'
a + ' ' + b
print('{} {}'.format(a, b))

# %%
a = 'Hello' 'World'
a

# %%
s = ''
for p in parts:
    s += p
print(s)

# %%
data = ['ACME', 50, 91.1]
','.join(str(d) for d in data)
print(a + ':' + b + ':')
print(':'.join([a, b]))
print(a, b, sep=':')

# %%
# Interpolating Variables in Strings
s = '{name} has {n} messages.'
s.format(name='Guido', n=37)

# %%
name = 'Guido'
n = 37
s.format_map(vars())

# %%


class Info:
    """Write a class."""

    def __init__(self, name, n):
        """Init."""
        self.name = name
        self.n = n


a = Info('Guido', 37)
s.format_map(vars(a))

# %%
name = 'Guido'
n = 37
'%(name) has %(n) messages.' % vars()

# %%
s = string.Template('$name has $n messages.')
s.substitute(vars())

# %%
# Reformatting text to a fixed number of columns
s = "Look into my eyes, look into my eyes, the eyes, the eyes, \
the eyes, not around the eyes, don't look around the eyes, \
look into my eyes, you're under."
s
print(textwrap.fill(s, 70))
print(textwrap.fill(s, 40))
print(textwrap.fill(s, 40, initial_indent='  '))
print(textwrap.fill(s, 40, subsequent_indent='  '))

# %%
# Handling HTML and XML Entities in Text
s = 'Elements are written as "<tag>text</tag>".'
s
print(html.escape(s))
print(html.escape(s, quote=False))

# %%
s = 'Spicy Jalapeño'
s.encode('ascii', errors='xmlcharrefreplace')

# %%
s = 'Spicy &quot;Jalape&#241;o&quot.'
p = HTMLParser()
p.unescape(s)


# %%
# Performing Text Operations on Byte Strings
data = b'Hello World'
data[0:5]
data.startswith(b'Hello')
data.split()
data.replace(b'Hello', b'Hello Cruel')

# %%
data = bytearray(b'Hello World')
data[0:5]
data.split()
data.startswith(b'Hello')
data.replace(b'Hello', b'Hello Cruel')

# %%
data = b'FOO:BAR,SPAM'
re.split('[:,]', data)

re.split(b'[:,]', data)
re.split(b'[:,]', data)

# %%
a = 'Hello World'
a[0]
a[1]

# %%
b = b'Hello World'
b[0]
b[1]
print(b)
print(b.decode('ascii'))
