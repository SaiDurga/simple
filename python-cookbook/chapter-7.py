"""Functions."""

from functools import partial

# %%
# Writing Functions That Accept Any Number of Arguments


def avg(first, *rest):
    """Write function with many arguments."""
    return (first + sum(rest)) / (1 + len(rest))


avg(1, 2)
avg(1, 2, 3)

# %%
# Writing Functions That Only Accept Keyword Arguments


def recv(maxsize, *, block):
    """Receive a message"""
    pass


recv(1024, block=True)


def mininum(*values, clip=None):
    """Clip."""
    m = min(values)
    if clip is not None:
        m = clip if clip > m else m
    return m


mininum(1, 5, 2, -5, 10)
mininum(1, 5, 2, -5, 10, clip=0)
mininum(1, 2, 3, 4, 5, clip=10)

# %%
# Attaching Informational Metadata to Function Arguments


def add(x: int, y: int) -> int:
    """Annotate a function."""
    return x + y


add(2, 3)
add(2.1, 3.2)
add.__annotations__

# %%
# Returning Multiple Values from a Function


def myfun():
    """return multiple values."""
    return 1, 2, 3


a, b, c = myfun()
a
b
c

a = (1, 2)
a
b = 1, 2
b
x = myfun()
x

# %%
# Defining Functions with Default Arguments


def spam(a, b=42):
    """Function with default arguments."""
    print(a, b)


spam(1)
spam(1, 2)

# %%
_no_value = object()


def spam1(a, b=_no_value):
    """Use novalue."""
    if b is _no_value:
        print("no b value supplied")


spam1(1)
spam1(1, 2)
spam1(1, None)

# %%
x = 42


def spam(a, b=x):
    """Default arguments."""
    print(a, b)


spam(1)
x = 23
spam(1)

# %%


def spam(a, b=[]):
    """Another way."""
    print(b)
    return b


x = spam(1)
x
x.append(99)
x.append('Yow!')
x
spam(1)

# %%
# Defining Anonymous or Inline Functions

add = lambda x, y: x + y
add(2, 3)
add('hello', 'world')

names = ['David Beazley', 'Brian Jones', 'Raymond Hettinger', 'Ned Batchelder']
sorted(names, key=lambda name: name.split()[-1].lower())

# %%
# Capturing Variables in Anonymous Functions
x = 10
a = lambda y: x + y
x = 20
b = lambda y: x +y
a(10)
b(10)
x = 15
a(10)
x = 3
a(10)

x = 10
a = lambda y, x=x: x + y
x = 20
b = lambda y, x=x: x + y
a(10)
b(10)

# %%
funcs = [lambda x: x+n for n in range(5)]
for f in funcs:
    print(f(0))

funcs = [lambda x, n=n: x+n for n in range(5)]
for f in funcs:
    print(f(0))

# %%
# Making an N-Argument Callable Work As a Callable with Fewer Arguments


def spam(a, b, c, d):
    """Few arguments."""
    print(a, b, c, d)


s1 = partial(spam, 1)
s1(2, 3, 4)
s1(4, 5, 6)
s2 = partial(spam, d=42)
s2(1, 2, 3)
s2(4, 5, 6)
s3 = partial(spam, 1, 2, d=42)
s3(3)
s3(4)
s3(10)

# %%
# Replacing Single Method Classes with Functions


class avrg:
    """Class with single method."""

    def __init__(self, value1):
        """Init."""
        self.value1 = value1

    def aver(self, value2):
        """Average method."""
        return (self.value1 + value2)/2


a = avrg(2)
a.aver(3)
a.aver(4)

# %%


def first(n1):
    """First function."""
    def second(n2):
        """Second function."""
        return (n1 + n2)/2
    return second


first(2)(3)
first(2)(4)

# %%
# Carrying Extra State with Callback Functions


def apply_async(func, args, *, callback):
    """Compute the result."""
    result = func(*args)
    callback(result)


def add(x, y):
    """Add x and y."""
    return x + y


def printfn(result):
    """Print the result."""
    print(result)


apply_async(add, (2, 3), callback=printfn)

# %%
# Accessing Variables Defined Inside a Closure
# (runs faster than using a normal class definition)


def sample():
    """Write the sample function."""
    n = 0

    def func():
        """Print n value."""
        print('n=', n)

    def get_n():
        """Get the value of n."""
        return n

    def set_n(value):
        """Set the value of n."""
        nonlocal n
        n = value

    func.get_n = get_n
    func.set_n = set_n
    return func


f = sample()
f()
f.set_n(10)
f.get_n()
