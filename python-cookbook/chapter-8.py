"""Classes and objects."""
import time
import math
import operator

# %%
# Changing the String Representation of Instances


class Pair:
    """Change the string representation."""

    def __init__(self, x, y):
        """Init."""
        self.x = x
        self.y = y

    def __repr__(self):
        """Represent."""
        return 'Pair({0.x!r}, {0.y!r})'.format(self)

    def __str__(self):
        """String."""
        return '({0.x!s}, {0.y!s})'.format(self)


p = Pair(3, 4)
p
print(p)

# %%
# Customizing String Formatting
_formats = {
            'ymd': '{d.year}-{d.month}-{d.day}',
            'mdy': '{d.month}/{d.day}/{d.year}',
            'dmy': '{d.day}/{d.month}/{d.year}'
}


class Date:
    """Customize string formatting."""

    def __init__(self, year, month, day):
        """Init."""
        self.year = year
        self.month = month
        self.day = day

    def __format__(self, code):
        """Format."""
        if code == '':
            code = 'ymd'
        fmt = _formats[code]
        return fmt.format(d=self)


d = Date(2018, 19, 4)
d
format(d)
format(d, 'mdy')
'The date is {:ymd}'.format(d)
'The date is {:mdy}'.format(d)

# %%
# Calling a Method on a Parent Class


class A:
    """Call a method in super class."""

    def spam(self):
        """Spam in super class."""
        print('A.spam')


class B(A):
    """Subclass."""

    def spam(self):
        """Spam in subclass."""
        print('B.spam')
        super().spam()


a = A()
a.spam()
b = B()
b.spam()

# %%


class A:
    """Superclass."""

    def __init__(self):
        """Init in super class."""
        self.x = 0
        print('A.__init__')


class B(A):
    """Subclass."""

    def __init__(self):
        """Init in subclass."""
        super().__init__()
        self.y = 1
        print('B.__init__')


a = A()
a.__init__()
b = B()
b.__init__()
# handling of the __init__() method to make sure that parents are properly
# initialized
# super() overrides any of Python’s special methods
# %%


class Base:
    """Base class."""

    def __init__(self):
        """Init in base class."""
        print('Base.__init__')


class A(Base):
    """Class A."""

    def __init__(self):
        """Init in class A."""
        Base.__init__(self)
        print('A.__init__')


class B(Base):
    """Class B."""

    def __init__(self):
        """Init in class C."""
        Base.__init__(self)
        print('B.__init__')


class C(A, B):
    """Class C."""

    def __init__(self):
        """Init in class C."""
        A.__init__(self)
        B.__init__(self)
        print('C.__init__')


c = C()

# %%


class Base:
    """Base class."""

    def __init__(self):
        """Init in base class."""
        print('Base.__init__')


class A(Base):
    """Class A."""

    def __init__(self):
        """Init in class A."""
        super().__init__()
        print('A.__init__')


class B(Base):
    """Class B."""

    def __init__(self):
        """Init in class C."""
        super().__init__()
        print('B.__init__')


class C(A, B):
    """Class C."""

    def __init__(self):
        """Init in class C."""
        super().__init__()
        print('C.__init__')


c = C()
C.__mro__

# %%


class A:
    """Class A."""

    def spam(self):
        """Spam in class A."""
        print('A.spam')
        super().spam()


a = A()
# a.spam()


class B:
    """Class B."""

    def spam(self):
        """Spam in class b."""
        print("b.spam")


class C(A, B):
    """Class C."""

    pass


c = C()
c.spam()

# %%
# Extending a property in a subclass.


class Person:
    """Person class."""

    def __init__(self, name):
        """Init."""
        self.name = name

    @property
    def name(self):
        """Name in get function."""
        return self._name

    @name.setter
    def name(self, value):
        """name in setter function."""
        if not isinstance(value, str):
            raise TypeError('Expected a string')
            self._name = value

    @name.deleter
    def name(self):
        raise AttributeError("Can't delete attribute")


class SubPerson(Person):
    """Sub Person class."""

    @property
    def name(self):
        """Name in subclass."""
        print('Getting name')
        return super().name

    @name.setter
    def name(self, value):
        """Set in sub class."""
        print('Setting name to', value)
        super(SubPerson, SubPerson).name.__set__(self, value)

    @name.deleter
    def name(self):
        """Delete in sub class."""
        print('Deleting name')
        super(SubPerson, SubPerson).name.__delete__(self)


s = SubPerson('Guido')
s.name
s.name = 'Larry'
# s.name = 42
s.name

# %%
# Delegating attribute access


class A:
    """Class A."""

    def spam(self, x):
        """Spam in class A."""
        pass

    def foo(self):
        """Foo on class A."""
        pass


class B:
    """Class B."""

    def __init__(self):
        """Init in class B."""
        self._a = A()

    def bar(self):
        """Bar in class B."""
        pass

    def __getattr__(self, name):
        """Get attr in class B."""
        return getattr(self._a, name)


b = B()
b.bar()
b.spam(42)

# %%
# Defining More Than One Constructor in a Class


class Date:
    """More than one constructor."""

    def __init__(self, year, month, day):
        """Primary constructor."""
        self.year = year
        self.month = month
        self.day = day

        @classmethod
        def today(cls):
            """Alternate constructor."""
            t = time.localtime()
            return cls(t.tm_year, t.tm_mon, t.tm_mday)


a = Date(2018, 4, 19)
b = Date.today()

# %%
# Creating an instance without invoking init


class Date:
    """Invoke without init."""

    def __init__(self, year, month, day):
        """Init."""
        self.year = year
        self.month = month
        self.day = day


d = Date.__new__(Date)
d
d.year
d.month

data = {'year': 2012, 'month': 8, 'day': 29}
for key, value in data.items():
    setattr(d, key, value)

d.year
d.month

# %%
# calling the method on an object given the name as a string


class Point:
    """Class point."""

    def __init__(self, x, y):
        """Init."""
        self.x = x
        self.y = y

    def __repr__(self):
        """Represet method."""
        return 'Point({!r:},{!r:})'.format(self.x, self.y)

    def distance(self, x, y):
        """Find distance method."""
        return math.hypot(self.x - x, self.y - y)


p = Point(2, 3)
d = getattr(p, 'distance')(0, 0)
operator.methodcaller('distance', 0, 0)(p)

# %%
points = [Point(1, 2), Point(3, 0), Point(10, -3), Point(-5, -7), Point(3, 2)]
points
points.sort(key=operator.methodcaller('distance', 0, 0))
