"""Numbers, Dates and Times."""

from decimal import Decimal
from decimal import localcontext
import math
import cmath
from fractions import Fraction
import numpy as np
import numpy.linalg as nl
import random as r
from datetime import timedelta
from datetime import datetime
from pytz import timezone

round(1.23, 1)
round(1.27, 1)
round(-1.27, 1)
round(1.253617, 4)
round(1.253617, 3)
round(1.5)
round(2.5)
round(3.5)
round(4.5)
round(4.7)

# %%
a = 1627731
round(a, -1)
round(a, -2)
round(a, -3)

# %%
x = 1.23456
format(x, '0.2f')
format(x, '0.3f')
format(x, '0.4f')

# %%
# Performing Accurate Decimal Calculations
a = 2.1
b = 4.2
c = a + b
c
c == 6.3

# %%
a = Decimal('4.2')
b = Decimal('2.1')
a + b
print(a + b)
(a + b) == Decimal('6.3')

# %%
a = Decimal('1.3')
b = Decimal('1.7')
print(a / b)

# %%
with localcontext() as ctx:
    ctx.prec = 3
    print(a / b)

# %%
with localcontext() as ctx:
    ctx.prec = 50
    print(a / b)

# %%
nums = [1.23e+18, 1, -1.23e+18]
sum(nums)

# %%
math.fsum(nums)

# %%
# Formatting Numbers for Output
x = 1234.56789
format(x, '0.2f')
format(x, '>10.1f')
format(x, '>10.1f')
format(x, '<10.1f')
format(x, '^10.1f')
format(x, '0.1f')
format(x, ',')
format(x, '0,.1f')
format(x, 'e')
format(x, '0.2E')
format(x, '0.3E')

# %%
# Working with Binary, Octal, and Hexadecimal Integers
x = 1234
bin(x)
oct(x)
hex(x)

# %%
format(x, 'b')
format(x, 'o')
format(x, 'x')

# %%
x = -1234
format(x, 'b')
format(x, 'x')

# %%
format(2**32 + x, 'b')
format(2**32 + x, 'x')

# %%
int('4d2', 16)
int('10011010010', 2)

# %%
# Packing and Unpacking Large Integers from Bytes
data = b'\x00\x124V\x00x\x90\xab\x00\xcd\xef\x01\x00#\x004'
len(data)
int.from_bytes(data, 'little')

x = 94522842520747284487117727783387188
x.to_bytes(16, 'big')

# %%
# Performing Complex-Valued Math
a = complex(2, 4)
b = 3 - 5j
a
b

# %%
a.real
a.imag
a.conjugate()
a+b
a-b
a*b
a/b

# %%
cmath.sin(a)
cmath.cos(a)
cmath.exp(a)
cmath.sqrt(-1)

# %%
# Working with Infinity and NaNs
a = float('inf')
b = float('-inf')
c = float('nan')
a
b
c
math.isinf(a)
math.isinf(b)
math.isnan(c)

# %%
a+45
a*10
a/10
10/a
a/a
a+b
a-b

# %%
c = float('nan')
c+23
c/23
c*2
math.sqrt(c)

# %%
c = float('nan')
d = float('nan')
c == d
c is d

# %%
# Calculating with Fractions
a = Fraction(5, 4)
a
b = Fraction(7, 16)
print(b)
print(a + b)
print(a*b)

# %%
# get numerator and denominator
c = a * b
c.numerator
c.denominator

# %%
# convert to float
float(c)

# %%
print(c.limit_denominator(8))

# %%
# convert float to fraction
x = 3.75
y = Fraction(*x.as_integer_ratio())
y
print(y)

# %%
# Calculating with Large Numerical Arrays
x = [1, 2, 3, 4]
y = [5, 6, 7, 8]
x * 2
x + y
x+10

# %%
ax = np.array([1, 2, 3, 4])
ax
ay = np.array([5, 6, 7, 8])
ay
ax * 2
ax + 10
ax + ay
ax * ay

# %%


def f(x):
    """Return values."""
    return 3*x**2 - 2*x + 7


f(ax)
np.sqrt(ax)
np.cos(ax)

# %%
grid = np.zeros(shape=(10, 10), dtype=float)
grid

grid += 10
grid

np.sin(grid)

# %%
# multi dimensions
a = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
a

# %%
# select a row
a[1]

# %%
# select a column
a[:, 1]

# %%
# select a subregion
a[1:3, 1:3]

# %%
# change subregion
a[1:3, 1:3] += 10
a

# %%
a + [100, 101, 102, 103]
a

# %%
# conditional assignment
np.where(a < 10, a, 10)

# %%
# Performing Matrix and Linear Algebra Calculations
m = np.matrix([[1, -2, 3], [0, 4, 5], [7, 8, -9]])
m

# %%
# return transpose
m.T

# %%
# return inverse
m.I

# %%
# create a vector and multiply the matrix with it
v = np.matrix([[2], [3], [4]])
v

m*v

# %%
# find determinant
nl.det(m)

# %%
# find eigen values
nl.eigvals(m)

# %%
# solve for x in mx =v
x = nl.solve(m, v)
x
m * x
v

# %%
# Picking Things at Random
values = [1, 2, 3, 4, 5, 6]
r.choice(values)
r.choice(values)
r.choice(values)
r.choice(values)

# %%
# select n items
r.sample(values, 2)
r.sample(values, 2)
r.sample(values, 3)
r.sample(values, 4)

# %%
# Shuffle numbers
r.shuffle(values)
values
r.shuffle(values)
values

# %%
# produce random integers
r.randint(0, 10)
r.randint(0, 10)
r.randint(0, 10)
r.randint(0, 100)
r.randint(0, 10)

# %%
# produce uniform floating-point values in the range 0 to 1,
r.random()
r.random()

# %%
# get N random-bits expressed as an integer
r.getrandbits(200)

# %%
# Converting Days to Seconds, and Other Basic Time Conversions
a = timedelta(days=2, hours=6)
a
a.seconds
b = timedelta(hours=4.5)
b
c = a + b
c.days
c.seconds
c.seconds/3600
c.total_seconds()/3600

# %%
a = datetime(2012, 9, 23)
print(a + timedelta(days=10))
a
b = datetime(2012, 12, 21)
d = b - a
d.days
now = datetime.today()
now
print(now + timedelta(minutes=10))

# %%
a = datetime(2012, 3, 1)
b = datetime(2012, 2, 28)
a - b
(a - b).days
c = datetime(2013, 3, 1)
d = datetime(2013, 2, 28)
(c - d).days

# %%
# Determining Last Friday’s Date
weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday',
            'Sunday']
weekdays


def get_previous_byday(dayname, start_date=None):
    if start_date is None:
        start_date = datetime.today()
    day_num = start_date.weekday()
    day_num_target = weekdays.index(dayname)
    days_ago = (7 + day_num - day_num_target) % 7
    if days_ago == 0:
        days_ago = 7
    target_date = start_date - timedelta(days=days_ago)
    return target_date


datetime.today()
get_previous_byday('Monday')
get_previous_byday('Tuesday')
get_previous_byday('Friday')
get_previous_byday('Sunday', datetime(2012, 12, 21))

# %%
d = datetime.now()
print(d)

# %%
# Converting Strings into Datetimes
text = '2012-09-20'
y = datetime.strptime(text, '%Y-%m-%d')
z = datetime.now()
diff = z - y
diff

# %%
z
nice_z = datetime.strftime(z, '%A %B %d %Y')
nice_z

# %%
# Manipulating Dates Involving Time Zones
d = datetime(2012, 12, 21, 9, 30, 0)
print(d)

central = timezone('US/Central')
loc_d = central.localize(d)
loc_d

# %%
bang_d = loc_d.astimezone(timezone('Asia/Kolkata'))
print(bang_d)
