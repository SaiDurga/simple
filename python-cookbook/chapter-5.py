"""Files and I/O."""
import os.path
import mmap
import os
import time
import sys

# %%
# Reading and Writing Text Data
with open('somefile.txt', 'wt') as f:
    f.write("hi\n")
    f.write("hello\n")

with open('somefile.txt', 'rt') as f:
    for line in f:
        print(line)

with open('somefile.txt', 'rt', newline='') as f:
    for line in f:
        print(line)

with open('somefile.txt', 'rt', newline='') as f:
    f.read()

# %%
# Printing to a File
with open('somefile.txt', 'rt') as f:
    print('Hello World!', file=f)

# %%
# Printing with a Different Separator or Line Ending
print('ACME', 50, 91.5)
print('ACME', 50, 91.5, sep=',')
print('ACME', 50, 91.5, sep=',', end='!!\n')

for i in range(5):
    print(i)

for i in range(5):
    print(i, end='\t')

# %%
print(','.join(['ACME', '50', '91.5']))

row = ('ACME', 50, 91.5)
print(','.join(str(x) for x in row))
print(*row, sep=',')

# %%
# Reading and Writing Binary Data
# with open('somefile.bin', 'rb') as f:
# with open('somefile.bin', 'wb') as f:
t = 'Hello World'
t[0]
for c in t:
    print(c)

# %%
b = b'Hello World'
b[0]
for c in b:
    print(c)

# %%
# Reading binary data into mutable read_into_buffer


def read_into_buffer(filename):
    """Read into buffer."""
    buf = bytearray(os.path.getsize(filename))
    with open(filename, 'rb') as f:
        f.readinto(buf)
    return buf


with open('sample.bin', 'wb') as f:
    f.write(b'Hello World')

buf = read_into_buffer('sample.bin')
buf
buf[0:5]
buf

with open('newsample.bin', 'wb') as f:
    f.write(buf)

buf
m1 = memoryview(buf)
m1
m2 = m1[-5:]
m2
m2[:] = b'WORLD'
buf

# %%
# Memory Mapping Binary Files


def memory_map(filename, access=mmap.ACCESS_WRITE):
    """Map memory."""
    size = os.path.getsize(filename)
    fd = os.open(filename, os.O_RDWR)
    return mmap.mmap(fd, size, access=access)


size = 1000000
with open('data', 'wb') as f:
    f.seek(size-1)
    f.write(b'\x00')

m = memory_map('data')
len(m)
m[0:10]
m[0]
m[0:11] = b'Hello World'
m.close()

with open('data', 'rb') as f:
    print(f.read(11))

with memory_map('data') as m:
    print(len(m))
    print(m[0:10])

m.closed

# %%
# Manipulating Pathnames
path = '/Users/beazley/Data/data.csv'

# %%
# last component of the path
os.path.basename(path)

# %%
# directory name
os.path.dirname(path)

# %%
# Join path components together
os.path.join('tmp', 'data', os.path.basename(path))

# %%
# Expand the user's home directory
path = '~/Data/data.csv'
os.path.expanduser(path)

# %%
# Split the file extension
os.path.splitext(path)

# %%
# Testing for the Existence of a File
os.path.exists('/etc/passwd')
os.path.exists('/tmp/spam')
os.path.isfile('/etc/passwd')
os.path.isdir('/etc/passwd')
os.path.realpath('/usr/local/bin/python3')
os.path.getsize('/etc/passwd')
os.path.getmtime('/etc/passwd')
time.ctime(os.path.getmtime('/etc/passwd'))

# %%
# getting a directory listing
os.listdir()

# %%
# Bypassing filename encoding
sys.getfilesystemencoding()

with open('jalape\xf1o.txt', 'w') as f:
    f.write('Spicy!')

os.listdir('.')

# %%
# Printing Bad Filenames


def bad_filename(filename):
    """Print bad files."""
    return repr(filename)[1:-1]


files = os.listdir('.')
files

for name in files:
    print(name)
