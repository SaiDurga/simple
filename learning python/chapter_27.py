"""Clas Coding Basics."""

# %%


class FirstClass:
    """First class."""

    def setdata(self, value):
        """Set data."""
        self.data = value

    def display(self):
        """Display the data."""
        print(self.data)


x = FirstClass()
y = FirstClass()

x.setdata("King Arthur")
y.setdata(3.14159)

x.display()
y.display()

x.data = "new value"
x.display()

# %%
x.anothername = "spam"
x.anothername

# %%
# Classes Are Customized by Inheritance


class SecondClass(FirstClass):
    """Second class."""

    def display(self):
        """Display in second class."""
        print('Current value = "%s"' % self.data)


z = SecondClass()
z.setdata(42)
z.display()
x.display()

# %%
# Classes Are Attributes in Modules
# Classes Can Intercept Python Operators


class ThirdClass(SecondClass):
    """Third class."""

    def __init__(self, value):
        """self."""
        self.data = value

    def __add__(self, other):
        """Add function."""
        return ThirdClass(self.data + other)

    def __str__(self):
        """Print function."""
        return '[ThirdClass: %s]' % self.data

    def mul(self, other):
        """Multiply."""
        self.data *= other


# %%
a = ThirdClass('abc')
a.display()
print(a)

# %%
b = a + 'xyz'
b.display()
a.mul(3)
print(a)

# %%
# The World’s Simplest Python Class


class rec:
    """Empty namespace object."""

    pass


rec.name = 'Bob'
rec.age = 40

# %%
print(rec.name)
print(rec.age)

# %%
x = rec()
y = rec()

x.name, y.name

# %%
x.name = 'Sue'
rec.name, x.name, y.name

# %%
list(rec.__dict__.keys())
list(name for name in rec.__dict__ if not name.startswith('__'))
list(x.__dict__.keys())
list(y.__dict__.keys())

# %%
x.name, x.__dict__['name']
x.age
x.__dict__['age']

# %%
x.__class__
rec.__bases__

# %%


def uppername(obj):
    """Try function outside class."""
    return obj.name.upper()


# %%
uppername(x)
rec.method = uppername
x.method()
y.method()
rec.method(x)

# %%
# Records Revisited: Classes Versus Dictionaries
rec = ('Bob', 40.5, ['dev', 'mgr'])
print(rec[0])

rec = {}
rec['name'] = 'Bob'
rec['age'] = 40.5
rec['jobs'] = ['dev', 'mgr']
print(rec['name'])

# %%


class rec:
    """Empty namespace object."""

    pass


rec.name = 'Bob'
rec.age = 40.5
rec.jobs = ['dev', 'mgr']

print(rec.name)

# %%

pers1 = rec()
pers1.name = 'Bob'
pers1.jobs = ['dev', 'mgr']
pers1.age = 40.5
pers2 = rec()
pers2.name = 'Sue'
pers2.jobs = ['dev', 'cto']
pers1.name, pers2.name

# %%


class Person:
    """Class for person."""

    def __init__(self, name, jobs, age=None):
        """Self."""
        self.name = name
        self.jobs = jobs
        self.age = age

    def info(self):
        """Info."""
        return (self.name, self.jobs)


rec1 = Person('Bob', ['dev', 'mgr'], 40.5)
rec2 = Person('Sue', ['dev', 'cto'])
rec1.jobs, rec2.info()
