"""Comprehensions and Generators."""
import numpy as np
# %%
ord('s')
ord('t')

# %%
res = []
for x in 'spam':
    res.append(ord(x))

res

# %%
# apply function to a sequence
res = list(map(ord, 'spam'))

# %%
# apply expression to a sequence
res = [ord(x) for x in 'spam']
res

# %%
[x ** 2 for x in range(10)]
list(map((lambda x: x ** 2), range(10)))

# %%
# Adding nests and nested loops: filter
[x for x in range(5) if x % 2 == 0]

list(filter((lambda x: x % 2 == 0), range(5)))

res = []
for x in range(5):
    if x % 2 == 0:
        res.append(x)

res
# %%
[x ** 2 for x in range(10) if x % 2 == 0]
list(map((lambda x: x**2), filter((lambda x: x % 2 == 0), range(10))))

# %%
res = [x + y for x in [0, 1, 2] for y in [100, 200, 300]]
res

[x + y for x in 'spam' for y in 'SPAM']

# %%
[x + y for x in 'spam' if x in 'sm' for y in 'SPAM' if y in ('P', 'A')]
[(x, y) for x in range(5) if x % 2 == 0 for y in range(5) if y % 2 == 1]

# %%
# Example: List Comprehensions and Matrixes
M = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
M[1]
M[1][2]

# %%
[row[1] for row in M]

# %%
[M[i][i] for i in range(len(M))]
[M[i][len(M)-1-i] for i in range(len(M))]

# %%
[col + 10 for row in M for col in row]
[[col + 10 for col in row] for row in M]

# %%
# multiplying M and N
N = [[2, 2, 2], [3, 3, 3], [4, 4, 4]]
[M[row][col] * N[row][col] for row in range(3) for col in range(3)]
[[M[row][col] * N[row][col] for col in range(3)] for row in range(3)]

# %%
# Generator Functions and Expressions
# Generator Functions: yield Versus return


def gensquares(N):
    """Use gen."""
    for i in range(N):
        yield i ** 2


for i in gensquares(5):
    print(i, end=' : ')

#  %%
x = gensquares(5)
next(x)
next(x)
next(x)
next(x)
next(x)

# %%
y = gensquares(5)
iter(y) is y
next(y)

# %%


def buildsquares(n):
    """Build squares."""
    res = []
    for i in range(n): res.append(i ** 2)
    return res

buildsquares(5)

# %%


def ups(line):
    """Use gens."""
    for sub in line.split(','):
        yield sub.upper()

x = ups('aaa,bbb,ccc')
x
next(x)
next(x)
next(x)
next(x)

# %%
tuple(ups('aaa,bbb,ccc'))

# %%


def gen():
    """Use gen."""
    for i in range(10):
        X = yield i
        print(X)


G = gen()
next(G)
G.send(77)
G.send(88)

# %%
# Generator Expressions: Iterables Meet Comprehensions
[x ** 2 for x in range(4)]
(x ** 2 for x in range(4))
list(x ** 2 for x in range(4))

# %%
G = (x ** 2 for x in range(4))
iter(G) is G
next(G)
next(G)
next(G)
next(G)
next(G)
G

# %%
for num in (x ** 2 for x in range(4)):
    print('%s, %s' % (num, num / 2.0))

# %%
"".join(x.upper() for x in 'aaa,bbb,ccc'.split(','))
a, b, c = (x + '\n' for x in 'aaa,bbb,ccc'.split(','))

# %%
sum(x ** 2 for x in range(4))
sorted(x ** 2 for x in range(4))
sorted((x ** 2 for x in range(4)), reverse=True)

# %%
# Generator expressions versus map
list(map(abs, (−1, −2, 3, 4)))
list(abs(x) for x in (−1, −2, 3, 4))
list(map(lambda x: x * 2, (1, 2, 3, 4)))
list(x * 2 for x in (1, 2, 3, 4))

# %%
line = 'aaa,bbb,ccc'
''.join([x.upper() for x in line.split(',')])
''.join(x.upper() for x in line.split(','))
''.join(map(str.upper, line.split(',')))
''.join(x * 2 for x in line.split(','))
''.join(map(lambda x: x * 2, line.split(',')))

# %%
# nested Comprehensions
[x * 2 for x in [abs(x) for x in (−1, −2, 3, 4)]]
list(map(lambda x: x * 2, map(abs, (−1, −2, 3, 4))))
list(x * 2 for x in (abs(x) for x in (−1, −2, 3, 4)))

# %%
# Generator expressions versus filter
line = 'aa bbb c'
''.join(x for x in line.split() if len(x) > 1)
''.join(filter(lambda x: len(x) > 1, line.split()))

# %%
''.join(x.upper() for x in line.split() if len(x) > 1)
''.join(map(str.upper, filter(lambda x: len(x) > 1, line.split())))

# %%
''.join(x.upper() for x in line.split() if len(x) > 1)

# %%
# Generator Functions Versus Generator Expressions
G = (c * 4 for c in 'SPAM')
list(G)

# %%


def timesfour(S):
    """Use gen."""
    for c in S:
        yield c * 4


G = timesfour('spam')
list(G)

# %%
G = (c * 4 for c in 'SPAM')
I1 = iter(G)
next(I1)
next(I1)

G = timesfour('spam')
I1 = iter(G)
next(I1)
next(I1)

# %%
''.join(x.upper() for x in line.split() if len(x) > 1)


def gensub(line):
    """Use gen."""
    for x in line.split():
        if len(x) > 1:
            yield x.upper()


''.join(gensub(line))

# %%
# Generators Are Single-Iteration Objects
G = (c * 4 for c in 'SPAM')
iter(G) is G

G = (c * 4 for c in 'SPAM')
I1 = iter(G)
next(I1)
next(I1)
I2 = iter(G)
next(I2)

# %%
list(I1)
next(I2)
I3 = iter(G)
next(I3)
I3 = iter(c * 4 for c in 'SPAM')
next(I3)

# %%


def timesfour(S):
    """Use gen"""
    for c in S:
        yield c * 4


G = timesfour('spam')
iter(G) is G
I1, I2 = iter(G), iter(G)
next(I1)
next(I1)
next(I2)

# %%
L = [1, 2, 3, 4]
I1, I2 = iter(L), iter(L)
next(I1)
next(I1)
next(I2)
del L[2:]
next(I1)

# %%
# Generation in Built-in Types, Tools, and Classes
D = {'a': 1, 'b': 2, 'c': 3}
x = iter(D)
next(x)
next(x)

# %%
for key in D:
    print(key, D[key])

# %%
# Generators and function application


def f(a, b, c): print('%s, %s, and %s' % (a, b, c))


f(0, 1, 2)
f(*range(3))
f(*(i for i in range(3)))

# %%
D = dict(a='Bob', b='dev', c=40.5)
f(a='Bob', b='dev', c=40.5)
f(**D)
f(*D)
f(*D.values())

# %%
# Scrambling sequences
L, S = [1, 2, 3], 'spam'
for i in range(len(S)):
    S = S[1:] + S[:1]
    print(S, end=' ')
