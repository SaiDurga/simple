"""A More Realistic Example."""

# %%
# Step 1: Making Instances


class Person:
    """Class for person."""

    def __init__(self, name, job=None, pay=0):
        """Self."""
        self.name = name
        self.job = job
        self.pay = pay

    def lastName(self):
        """Return last name."""
        return self.name.split()[-1]

    def giveRaise(self, percent):
        """Raise salary."""
        self.pay = int(self.pay * (1 + percent))

    def __repr__(self):
        """Representation."""
        return '[Person: %s, %s]' % (self.name, self.pay)


class Manager(Person):
    """Class for manager."""

    def __init__(self, name, pay):
        """Another init in manager ."""
        Person.__init__(self, name, 'mgr', pay)

    def giveRaise(self, percent, bonus=.10):
        """Another giveraise."""
        Person.giveRaise(self, percent + bonus)


if __name__ == '__main__':
    bob = Person('Bob Smith')
    sue = Person('Sue Jones', job='dev', pay=100000)
    print(bob.name, bob.pay)
    print(sue.name, sue.pay)
    print(bob.lastName(), sue.lastName())
    sue.giveRaise(.10)
    print(sue.pay)
    tom = Manager('Tom Jones', 50000)
    tom.giveRaise(.10)
    print(tom.lastName())
    print(tom)



# %%
name = 'Bob Smith'
name.split()
name.split()[-1]

pay = 100000
pay *= 1.10
print('%.2f' % pay)
