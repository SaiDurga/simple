"""Exception Coding Details."""


def gobad(x, y):
    """Try/Else."""
    return x / y


def gosouth(x):
    """Try/Else."""
    print(gobad(x, 0))


gosouth(1)

# %%
# Catching Built-in Exceptions


def kaboom(x, y):
    """Kaboom."""
    print(x + y)


try:
    kaboom([0, 1, 2], 'spam')
except TypeError:
    print('Hello world!')
print('resuming here')

# %%
# The try/finally Statement
# Coding Termination Actions with try/finally


class MyError(Exception):
    """Try finally statement."""

    pass


def stuff(file):
    """Raise exception."""
    raise MyError()


file = open('data', 'w')

try:
    stuff(file)
finally:
    file.close()
    print('not reached')

# %%
# Unified try/except/finally
# exception raised and caught
print('EXCEPTION RAISED AND CAUGHT')
try:
    x = 'spam'[99]
except IndexError:
    print('except run')
finally:
    print('finally run')
print('after run')

# %%
print('NO EXCEPTION RAISED')
try:
    x = 'spam'[3]
except IndexError:
    print('except run')
finally:
    print('finally run')
print('after run')

# %%
print('NO EXCEPTION RAISED, WITH ELSE')
try:
    x = 'spam'[99]
except IndexError:
    print('except run')
else:
    print('else run')
finally:
    print('finally run')
print('after run')

# %%
print('EXCEPTION RAISED BUT NOT CAUGHT')
try:
    x = 1 / 0
except IndexError:
    print('except run')
finally:
    print('finally run')
print('after run')

# %%
# The raise Statement


class MyExc(Exception):
    """Raise statement."""

    pass


def func():
    """Raise exception."""
    raise MyExc('spam')


try:
    func()
except MyExc as X:
    print(X.args)

# %%
# Scopes and try expect variables
try:
    1/0
except Exception as X:
    print X


# %%
try:
    1 / 0
except Exception, X:
    print X


# %%
try:
    1 / 0
except Exception as X:
    print(X)
    Saveit = X

# %%
# Propagating Exceptions with raise
try:
    1 / 0
except Exception as E:
    raise TypeError('Bad') from E

# %%
try:
    try:
        raise IndexError()
    except Exception as E:
        raise TypeError() from E
except Exception as E:
    raise SyntaxError() from E

# %%
try:
    try:
        1 / 0
    except:
        badname
except:
    open('nonesuch')


# %%
# The assert statement.

def f(x):
    """Assert statement."""
    assert x < 0, 'x must be negative'
    return x ** 2


f(3)
