"""Chapter-19."""

import sys
from functools import reduce
import numpy as np
import operator, functools

# %%


def func(x, y, z):
    """Function using def."""
    return x + y + z


func(1, 2, 3)

# %%
# use lambda

f = (lambda x, y, z: x + y + z)
f(1, 2, 3)

# %%
# Defaults work on lambda arguments
x = (lambda a="fee", b="fie", c="foe": a + b + c)
x("wee")

# %%


def knights():
    """Scope lookup rules."""
    title = 'Sir'
    action = (lambda x: title + ' ' + x)
    return action


act = knights()
msg = act('robin')
msg
act

# %%
# Why Use lambda?
L = [lambda x: x ** 2, lambda x: x ** 3, lambda x: x ** 4]

for f in L:
    print(f(2))

print(L[0](3))

# %%
# Multiway branch switches: The finale
key = 'got'
({'already': (lambda: 2 + 2), 'got': (lambda: 2 * 4), 'one': (lambda: 2 ** 6)}
 [key]())

# %%


def f1(): return 2 + 2


def f2(): return 2 * 4


def f3(): return 2 ** 6


key = 'one'
{'already': f1, 'got': f2, 'one': f3}[key]()


# %%
lower = (lambda x, y: x if x < y else y)
lower('bb', 'aa')
lower('aa', 'bb')
lower(2, 3)
lower(3, 2)

# %%
showall = (lambda x: list(map(sys.stdout.write, x)))
t = showall(['spam\n', 'toast\n', 'eggs\n'])

# %%
showall = (lambda x: [sys.stdout.write(line) for line in x])
t = showall(('bright\n', 'side\n', 'of\n', 'life\n'))

# %%
# Scopes: lambdas Can Be Nested Too


def action(x):
    """Nest using lambda."""
    return (lambda y: x + y)


act = action(99)
act(22)
action(99)(22)

# %%
action = (lambda x: (lambda y: x + y))
action(99)(22)

# %%
# Functional Programming Tools
counters = [1, 2, 3, 4]
updated = []
for x in counters:
    updated.append(x+10)

updated

# %%


def inc(x): return x + 10


list(map(inc, counters))

# %%
list(map((lambda x: x + 3), counters))

# %%
list(map(pow, [1, 2, 3], [2, 3, 4]))

# %%
# Selecting Items in Iterables: filter
list(range(-5, 5))
list(filter((lambda x: x > 0), range(−5, 5)))

# %%
[x for x in range(−5, 5) if x > 0]

# %%
# Combining Items in Iterables: reduce
reduce((lambda x, y: x + y), [1, 2, 3, 4])
reduce((lambda x, y: x * y), [1, 2, 3, 4])

# %%
functools.reduce(operator.add, [2, 4, 6])
functools.reduce((lambda x, y: x + y), [2, 4, 6])
