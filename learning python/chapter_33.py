"""Exception Basics."""


def fetcher(obj, index):
    """Fetch function."""
    return obj[index]


x = 'spam'
fetcher(x, 3)

fetcher(x, 4)

# %%

try:
    fetcher(x, 4)
except IndexError:
    print('got exception')

# %%


def catcher():
    """Exception handler."""
    try:
        fetcher(x, 4)
    except IndexError:
        print('got exception')
    print('continuing')


catcher()

# %%
# Raising Exceptions

try:
    raise IndexError
except IndexError:
    print('got exception')

raise IndexError

# %%
# User-Defined Exceptions


class AlreadyGotOne(Exception):
    """User defined exceptions."""

    pass


def grail():
    """Define Exception."""
    raise AlreadyGotOne()


try:
    grail()
except AlreadyGotOne:
    print('got exception')


class Career(Exception):
    """User defined exceptions."""

    def __str__(self):
        """String."""
        return 'So I became a waiter...'


raise Career()

# %%
# Termination Actions
try:
    fetcher(x, 3)
finally:
    print('after fetch')

# %%
try:
    fetcher(x, 4)
finally:
    print('after fetch')
print('after fetch?')


# %%


def after():
    """Change the call."""
    try:
        fetcher(x, 3)
    finally:
        print('after fetch')
    print('after try?')


after()
