"""Chapter -17 -- Scopes."""

import builtins

X = 68


def func(a):
    """Test scope."""
    X = 63
    return X


X
func('a')

# %%


def func1(Y):
    """Add X and Y."""
    Z = X + Y
    return Z


func1(2)


def func2(Y):
    """Add X and Y."""
    X = 10
    Z = X + Y
    return Z


func2(2)


# %%
dir(builtins)
zip
builtins.zip
zip is builtins.zip
len(dir(builtins))

# %%
# The global Statement
a = 88


def fn():
    """Global statements."""
    global a
    a = 99


fn()
print(a)


# %%
# Program Design: Minimize Global Variables
b = 99


def fn_b1():
    """Minimize global variables."""
    global b
    b = 100


def fn_b2():
    """Minimixe global variables."""
    global b
    b = 101


b
fn_b1()
b
fn_b2()
b

# %%
# Scopes and nested functions
X = 99


def f1():
    """Function1."""
    X = 88

    def f2():
        """FUnction."""
        print(X)
    f2()


f1()

# %%


def f1():
    """Function1."""
    X = 88

    def f2():
        """Function2."""
        print(X)
    return f2


action = f1()
action()

# %%
# Factory Functions: Closures


def maker(N):
    """Make function."""
    def action(X):
        """Act function."""
        return X ** N
    return action


f = maker(2)
f(3)
f(4)

g = maker(3)
g(2)
g(4)

# %%


def maker(N):
    """Make another function."""
    return lambda X: X ** N


h = maker(3)
h(2)

# %%
# Retaining Enclosing Scope State with Defaults


def f1():
    """Function1."""
    X = 88

    def f2(X=X):
        """Function2."""
        print(X)
    f2()


f1()

# %%


def f1():
    """Function."""
    x = 88
    f2(x)


def f2(x):
    """Function2."""
    print(x)


f1()

# %%
# Nested scopes, defaults, and lambdas


def func():
    """Use lambda."""
    x = 4
    action = (lambda n: x ** n)
    return action


x = func()
print(x(2))


# %%


def func():
    """Use lamda."""
    x = 4
    action = (lambda n, x=x: x ** n)
    return action


y = func()
print(y(2))

# %%
# Loop variables may require defaults, not scopes


def makeActions():
    """Loop variables without defaults."""
    acts = []
    for i in range(5):
        acts.append(lambda x: i ** x)
    return acts


acts = makeActions()
acts[0]
acts[0](2)
acts[1](2)
acts[2](2)
acts[4](2)

# %%


def makeActions():
    """Loop variables defaults."""
    acts = []
    for i in range(5):
        acts.append(lambda x, i=i: i ** x)
    return acts


acts = makeActions()
acts[0]
acts[0](2)
acts[1](2)
acts[2](2)
acts[4](2)

# %%
# Arbitrary scope nesting


def f1():
    """Nested."""
    x = 99

    def f2():
        def f3():
            print(x)
        f3()
    f2()


f1()


# %%
# The nonlocal Statement in 3.X
# nonlocal in Action
def tester(start):
    """Test."""
    state = start

    def nested(label):
        """Nest."""
        print(label, state)
    return nested


F = tester(0)
F('spam')
F('ham')

# %%


def tester(start):
    """Test."""
    state = start

    def nested(label):
        """Nest."""
        print(label, state)
        state += 1
    return nested


F = tester(0)
F('spam')

# %%
# Using nonlocal for changes


def tester(start):
    """Test."""
    state = start

    def nested(label):
        """Nest."""
        nonlocal state
        print(label, state)
        state += 1
    return nested


F = tester(0)
F('spam')
F('ham')
F('eggs')

G = tester(32)
G('a')
G('b')
G('c')

# %%
# Boundary cases


def tester(start):
    def nested(label):
        nonlocal state
        state = 0
        print(label, state)
    return nested


# %%


def tester(start):
    def nested(label):
        global state
        state = 0
        print(label, state)
    return nested


F = tester(0)
F('a')
state

# %%
spam = 99


def tester():
    def nested():
        """Nest."""
        nonlocal spam
        print('Current=', spam)
        spam += 1
    return nested


# %%
# Why nonlocal? State Retention Options
# State with nonlocal: 3.X only


def tester(start):
    """Test."""
    state = start
    def nested(label):
        """Nest."""
        nonlocal state
        print(label, state)
        state += 1
    return nested


F = tester(0)
F('spam')
F.state

# %%
# State with Globals: A Single Copy Only


def tester(start):
    """Test function"""
    global state
    state = start

    def nested(label):
        """Nest function."""
        global state
        print(label, state)
        state += 1
    return nested


F = tester(0)
F('spam')
F('ham')
G = tester(42)
G('toast')
G('bread')
F('eggs')

# %%
# State with Classes: Explicit Attributes (Preview)


class tester:
    def __init__(self, start):
        """Test init"""
        self.state = start

    def nested(self, label):
        """Nest"""
        print(label, self.state)
        self.state += 1


F = tester(0)
F.nested('spam')
F.nested('ham')
G = tester(42)
G.nested('toast')
G.nested('bacon')
F.nested('eggs')
F.state

# %%
# State with Function Attributes: 3.X and 2.X


def tester(start):
    """"Test with function arguments."""
    def nested(label):
        """Nest with function arguments."""
        print(label, nested.state)
        nested.state += 1
    nested.state = start
    return nested


F = tester(0)
F('spam')
F('ham')
F.state
G = tester(42)
G('toast')
G('bacon')
F('eggs')
G.state
F is G

# %%
# State with mutables


def tester(start):
    def nested(label):
        print(label, state[0])
        state[0] += 1
    state = [start]
    return nested


F = tester(0)
F('spam')
F('ham')
F.state
G = tester(42)
G('toast')
G('bacon')
F('eggs')
G.state
