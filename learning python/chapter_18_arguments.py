"""Practice chapter-18 in learning python."""

# %%
# Arguments and Shared References


def f(a):
    """Arguments and shared references."""
    a = 99

b = 88
f(b)
print(b)

# %%
def changer(a, b):
    """.Arguments and shared refrences."""
    a = 2
    b[0] = 'spam'


X = 1
L = [1, 2]
changer(X, L)
X, L

# %%
# Avoiding Mutable Argument Changes
L1 =[3, 4]
changer(X, L1[:])
X, L1

# %%


def changer(a, b):
    """Second changer."""
    b = b[:]
    a = 2
    b[0] = 'spam'


L = [1, 2]
changer(X, tuple(L))

# %%
# Simulating Output Parameters and Multiple Results


def multiple(x, y):
    """Multiple function."""
    x = 2
    y = [3, 4]
    return x, y


X = 1
L = [1, 2]
y, L =multiple(X, L)
X, L

# %%
# Keyword and Default Examples


def f(a, b, c):
    """Keyword and default values."""
    print(a, b, c)


f(1, 2, 3)
# passed by position

# %%
# match by name
f(c=3, b=2, a=1)

# %%
# combine positional and keyword arguments
f(1, c=3, b=2)

# %%
# defaults


def f(a, b=2, c=3):
    """Default values."""
    print(a, b, c)


f(1)
f(a=1)
# use defaults

# %%
# override defaults
f(1, 4)
f(1, 4, 5)

# %%
# choose defaults
f(1, c=6)

# %%
# Combining keywords and defaults
# pass atleast two arguments, other are optional


def func(spam, eggs, toast=0, ham=0):
    """Two are required."""
    print((spam, eggs, toast, ham))


func(1, 2)
func(1, eggs=1, ham=1)
func(spam=1, eggs=0)
func(toast=1, eggs=2, spam=3)
func(1, 2, 3, 4)

# %%
# Arbitrary Arguments Examples


def f(*args):
    """Arbitrary arguments."""
    print(args)


f()
f(1)
f(1, 2, 3, 4)

# %%


def f(**args):
    """Keyword arguments."""
    print(args)


f()
f(a=1, b=2)

# %%


def f(a, *pargs, **kargs):
    """Keyword arguments."""
    print(a, pargs, kargs)


f(1, 2, 3, x=1, y=2)

# %%
# Unpacking arguments


def func(a, b, c, d):
    """Unpack arguments."""
    print(a, b, c, d)


args = (1, 2)
args += (3, 4)
func(*args)

# %%
args = {'a': 1, 'b': 2, 'c': 3}
args['d'] = 4
func(*args)
func(**args)

# %%
# combine normal, positional, keyword arguments
func(*(1, 2), **{'d': 4, 'c': 3})
func(1, *(2, 3), **{'d': 4})
func(1, c=3, *(2,), **{'d': 4})
func(1, *(2, 3), d=4)
func(1, *(2,), c=3, **{'d': 4})

# %%
# Applying functions generically


def tracer(func, *pargs, **kargs):
    """Trace function."""
    print('calling:', func.__name__)
    return func(*pargs, **kargs)


def func(a, b, c, d):
    """Function."""
    return a+b+c+d


def func2(a, b, c):
    """Function2."""
    return a * b * c


print(tracer(func, 1, 2, c=3, d=4))
print(tracer(func2, 1, 2, c=3))

# %%
# The defunct apply built-in


def echo(*args, **kwargs):
    """Apply defunct example."""
    print(args, kwargs)


echo(1, 2, a=3, b=4)

# %%
pargs = (1, 2)
kargs = {'a': 3, 'b': 4}

# apply(echo, pargs, kargs)

echo(*pargs, **kargs)
echo(0, c=5, *pargs, **kargs)

# %%
# Python 3.X Keyword-Only Arguments


def kwonly(a, *b, c):
    """Keyword only arguments."""
    print(a, b, c)


kwonly(1, 2, c=3)
kwonly(a=1, c=3)
kwonly(1, 2, 3)

# %%


def kwonly(a, *, b, c):
    """Keyword only arguments."""
    print(a, b, c)


kwonly(1, c=3, b=2)
kwonly(c=3, b=2, a=1)
kwonly(1, 2, 3)

# %%


def kwonly(a, *, b='spam', c='ham'):
    """Keyword only arguments."""
    print(a, b, c)


kwonly(1)
kwonly(1, c=3)
kwonly(a=1)
kwonly(c=3, b=2, a=1)

# %%
# ordering rules
def f(a, *b, **d, c=6):
    """Order rules."""
    print(a, b, c, d)


# %%


def f(a, *b, c=6, **d):
    """Order rules."""
    print(a, b, c, d)


f(1, 2, 3, x=4, y=5)
f(1, 2, 3, x=4, y=5, c=7)

# %%
