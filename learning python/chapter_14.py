"""Chapter-14 in learning python."""

# Iterations: A first look

# %%
for x in [1, 2, 3, 4]: print(x ** 2)

# %%
for x in (1, 2, 3, 4): print(x ** 3, end=' ')

# %%
for x in 'spam': print(x * 2, end=' ')

# %%
# the iteration protocol: file iterators
f = open('script2.py', 'w')
f.write('import sys\n')
f.write('print(sys.path)\n')
f.write('x = 2\n')
f.write('print(x ** 32)\n')

open('script2.py').read()
print(open('script2.py').read())

f = open('script2.py')
f.readline()
f.readline()
f.readline()
f.readline()
f.readline()

# %%
f = open('script2.py')
f.__next__()
f.__next__()
f.__next__()
f.__next__()
f.__next__()


# %%
for line in open('script2.py'):
    print(line.upper(), end='')

# %%
for line in open('script2.py').readlines():
    print(line.upper(), end='')

# %%
f = open('script2.py')
while True:
    line = f.readline()
    if not line: break
    print(line.upper(), end='')

# %%
# Manual Iteration: iter and next
f = open('script2.py')
f.__next__()
f.__next__()
f.__next__()

# %%
f = open('script2.py')
next(f)
next(f)

# %%
L = [1, 2, 3]
It = iter(L)
It.__next__()
It.__next__()

# %%
f = open('script2.py')
iter(f) is f
iter(f) is f.__iter__()
f.__next__()

# %%
L = [1, 2, 3]
iter(L) is L
L.__next__()
It = iter(L)
It.__next__()
next(It)

# %%
# Manual iteration
L = [1, 2, 3]
for X in L:
    print(X ** 2, end=' ')

# %%
It = iter(L)
while True:
    try:
        X = next(It)
    except StopIteration:
        break
    print(X ** 2, end=' ')

# %%
# Other Built-in Type Iterables
D = {'a': 1, 'b': 2, 'c': 3}
for key in D.keys():
    print(key, D[key])

# %%
I = iter(D)
next(I)
next(I)
next(I)
next(I)

# %%
for key in D:
    print(key, D[key])

# %%
R = range(5)
R
It = iter(R)
next(It)
next(It)
next(It)
next(It)
next(It)
next(It)

# %%
# List Comprehensions: A First Detailed Look
L = [1, 2, 3, 4, 5]
for i in range(len(L)):
    L[i] += 10
L

# %%
L = [x+10 for x in L]
L

# %%
f = open('script2.py')
lines = f.readlines()
lines

# %%
lines = [line.rstrip() for line in lines]
lines

# %%
lines = [line.rstrip() for line in open('script2.py')]
lines

# %%
[line.upper() for line in open('script2.py')]
[line.rstrip().upper() for line in open('script2.py')]
[line.split() for line in open('script2.py')]
[line.replace(' ', '!') for line in open('script2.py')]
[('sys' in line, line[:5]) for line in open('script2.py')]

# %%
# Extended List Comprehension Syntax
lines = [line.rstrip() for line in open('script2.py') if line[0] == 'p']
lines

# %%
a = [3, 5, 1, 2, 7, 4, 8, 9]
a1 = [(x ** 2) for x in a if x < 5]
a1

# %%
[line.rstrip() for line in open('script2.py') if line.rstrip()[-1].isdigit()]

# %%
# for
[x + y for x in 'abc' for y in 'lmn']

# %%
# Other Iteration Contexts
map(str.upper, open('script2.py'))
list(map(str.upper, open('script2.py')))

# %%
sorted(open('script2.py'))
list(zip(open('script2.py'), open('script2.py')))
list(enumerate(open('script2.py')))
list(filter(bool, open('script2.py')))

# %%
list(open('script2.py'))
tuple(open('script2.py'))
'&&'.join(open('script2.py'))
a, b, c, d = open('script2.py')
a
b
a, *b = open('script2.py')
a, b
'y = 2\n' in open('script2.py')
'x = 2\n' in open('script2.py')

# %%
L = [11, 22, 33, 44]
L[1:3] = open('script2.py')
L

# %%

L = [11, 22, 33, 44]
L[1:3] = open('script2.py')
L

# %%
L = [11]
L.extend(open('script2.py'))
L

# %%
L = [11]
L.append(open('script2.py'))
L
list(L)

# %%
set(open('script2.py'))
{line for line in open('script2.py')}
{ix: line for ix, line in enumerate(open('script2.py'))}

# %%
{line for line in open('script2.py') if line[0] == 'p'}
{ix: line for (ix, line) in enumerate(open('script2.py')) if line[0] == 'p'}
list(line.upper() for line in open('script2.py'))

# %%
sum([3, 2, 1, 5, 4])
any(['spam', '', 'ni'])
all(['spam', '', 'ni'])
max([3, 2, 1, 5, 4])
min([3, 2, 1, 5, 4])

# %%


def f(a, b, c, d): print(a, b, c, d, sep='&')


f(1, 2, 3, 4)
f(*[1, 2, 3, 4])
f(*open('script2.py'))


# %%
# New Iterables in Python 3.X
