"""A More Realistic Example."""

# %%


class Person:
    """Class for person."""

    def __init__(self, name, job=None, pay=0):
        """Self."""
        self.name = name
        self.job = job
        self.pay = pay

    def lastName(self):
        """Return last name."""
        return self.name.split()[-1]

    def giveRaise(self, percent):
        """Raise salary."""
        self.pay = int(self.pay * (1 + percent))

    def __repr__(self):
        """Representation."""
        return '[Person: %s, %s]' % (self.name, self.pay)


class Manager(Person):
    """Class for manager."""

    def __init__(self, name, pay):
        """Another init in manager ."""
        Person.__init__(self, name, 'mgr', pay)

    def giveRaise(self, percent, bonus=.10):
        """Another giveraise."""
        Person.giveRaise(self, percent + bonus)

    def __getattr__(self, attr):
        """Use getattr."""
        return getattr(self.person, attr)



class Department:
    """Class for department."""

    def __init__(self, *args):
        """Another init in Department."""
        self.members = list(args)

    def addMember(self, person):
        """Add members."""
        self.members.append(person)

    def giveRaises(self, percent):
        """Raise pay."""
        for person in self.members:
            person.giveRaise(percent)

    def showAll(self):
        """Show all."""
        for person in self.members:
            print(person)


if __name__ == '__main__':
    bob = Person('Bob Smith')
    sue = Person('Sue Jones', job='dev', pay=100000)
    tom = Manager('Tom Jones', 50000)
    development = Department(bob, sue)
    development.addMember(tom)
    development.giveRaises(.10)
    development.showAll()
