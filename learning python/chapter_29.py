"""Class Coding Details."""
from abc import ABCMeta, abstractmethod


class SharedData:
    """Class for shared data."""

    spam = 42


x = SharedData()
y = SharedData()
x.spam
y.spam

# %%
SharedData.spam = 99
x.spam, y.spam, SharedData.spam

x.spam = 88
x.spam, y.spam, SharedData.spam

# %%


class MixedNames:
    """Class for mixed names."""

    data = 'spam'

    def __init__(self, value):
        """Self."""
        self.data = value

    def display(self):
        """Display message."""
        print(self.data, MixedNames.data)


x = MixedNames(1)
y = MixedNames(2)
x.display()
y.display()

# %%


class NextClass:
    """Class for next."""

    def printer(self, text):
        """Print message."""
        self.message = text
        print(self.message)


x = NextClass()
x.printer('instance call')
x.message
NextClass.printer(x, 'class call')
x.message

# %%
# inheritance


class Super:
    """Super class."""

    def method(self):
        """Write method in super class."""
        print('in Super.method')


class Sub:
    """Sub class."""

    def method(self):
        """Write method in sub class."""
        print('starting Sub.method')
        Super.method(self)
        print('ending Sub.method')


x = Super()
x.method()

y = Sub()
y.method()

# %%
# Class Interface Techniques


class Super:
    """Super class."""

    def method(self):
        """Write method in superclass."""
        print('in Super.method')

    def delegate(self):
        """Delegate."""
        self.action()


class Inheritor(Super):
    """Class for inheritor."""

    pass


class Replacer(Super):
    """Class for replacer."""

    def method(self):
        """Write method in Replacer."""
        print('in Replacer.method')


class Extender(Super):
    """Class for extender."""

    def method(self):
        """Write method in extender."""
        print('starting Extender.method')
        Super.method(self)
        print('ending Extender.method')


class Provider(Super):
    """Class for provider."""

    def action(self):
        """Action."""
        print('in Provider.action')


if __name__ == '__main__':
    for klass in (Inheritor, Replacer, Extender):
        print('\n' + klass.__name__ + '...')
        klass().method()
print('\nProvider...')
x = Provider()
x.delegate()

# %%


class Super(metaclass=ABCMeta):
    """Nother super class."""

    def delegate(self):
        """Delegate."""
        self.action()

    @abstractmethod
    def action(self):
        """Action."""
        pass


X = Super()


class Sub(Super):
    """Sub class."""

    pass


X = Sub()


class Sub(Super):
    """Sub class."""

    def action(self):
        """Action."""
        print('spam')


X = Sub()
X.delegate()
