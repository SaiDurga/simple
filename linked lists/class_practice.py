"""Practice class objects."""

# %%


class MyClass:
    """A simple example class."""

    i = 12345

    def f(self):
        """Test."""
        return 'hello world'


x = MyClass()
x.f
x.f()

x.counter = 1
while x.counter < 10:
    x.counter = x.counter * 2
print(x.counter)
del x.counter
x.counter = 2
x.counter
# %%


class Complex:
    """Another class."""

    def __init__(self, realpart, imagpart):
        """Find real and imaginary part."""
        self.r = realpart
        self.i = imagpart


c = Complex(1, 2)
c.r
c.i

# %%


class Complex2:
    """Another class."""

    def fu(self, realpart, imagpart):
        """Find real and imaginary part."""
        self.r = realpart
        self.i = imagpart


c2 = Complex2()
c2.fu(1, -2)

# %%


class Dog:
    """Class variable shared by all instances."""

    kind = 'canine'

    def __init__(self, name):
        """Function."""
        self.name = name


d1 = Dog('fido')
d2 = Dog('buddy')
d1.kind
d2.kind
d1.name
d2.name

# %%


class Dog:
    """Mistaken use of a class variable."""

    tricks = []

    def __init__(self, name):
        """Function."""
        self.name = name

    def add_trick(self, trick):
        """Add tricks."""
        self.tricks.append(trick)


d1 = Dog('foo')
d2 = Dog('buddy')
d1.tricks
d2.tricks
d1.add_trick(1)
d1.tricks
d2.tricks
d2.add_trick(2)
d2.tricks
d1.tricks


# %%


class Dog:
    """Create a new empty list for each dog."""

    def __init__(self, name):
        """Function1."""
        self.name = name
        self.tricks = []

    def add_trick(self, trick):
        """Function2."""
        self.tricks.append(trick)


d1 = Dog('foo')
d2 = Dog('buddy')
d1.tricks
d2.tricks
d1.add_trick(1)
d1.tricks
d2.tricks
d2.add_trick(2)
d2.tricks
d1.tricks
