"""Create LinkedCell class and then use it to create linked list."""


class LinkedCell:
    """Node implementation for linked list."""

    def __init__(self, v, n=-1):
        """Construct for LinkedCell."""
        self.value = v
        self.next = n
        self.length = 1

    def prepend(self, v):
        """Prepend given value to the linked list."""
        newc = LinkedCell(v)
        newc.next = self
        newc.length = self.length + 1
        return newc

    def len(self):
        """Calculate length of the linked list."""
        length = 0
        n = self
        while n != -1:
            length = length + 1
            n = n.next
        return length

    def seek(self, i):
        """Return the value at index."""
        n = self
        if i < 0 or i >= self.length:
            return -1
        while i > 0:
            i = i - 1
            n = n.next
        return n.value

    def assign(self, num, ind):
        """Assign given value to the given index."""
        n = self
        i = 0
        if ind < 0 or ind >= self.length:
            return -1
        while i != ind:
            i = i+1
            n = n.next
        n.value = num

    def insert(self, num, ind):
        """Insert given value to the given index."""
        n = self
        if ind <= 0 or ind >= self.length:
            return -1
        i = 0
        while i != ind-1:
            n = n.next
            n.length = n.length + 1
            i = i+1
        newc = LinkedCell(num)
        newc.next = n.next
        n.next = newc
        return self

    def remove(self, ind):
        """Remove element at the given index."""
        n = self
        i = 0
        if ind < 0 or ind >= self.length:
            return -1
        if ind == 0:
            n.value = n.next.value
        else:
            while i != ind-1:
                if n.next != -1:
                    n = n.next
                    i = i+1
        n.next = n.next.next

    def append(self, v):
        """Append in linked list."""
        n = self
        newc = LinkedCell(v)
        while n.next != -1:
            n = n.next
        n.next = newc
        self.length = self.length + 1
        return self

    def simple_traverse(self):
        """Traverse and print only node values."""
        n = self
        while n != -1:
            print('{} -> '.format(n.value), end='')
            n = n.next
        print('End')


class TestLinkedCell(object):
    """Test suite for LinkedCell."""

    def test_creation1(self):
        """Test __init__ functionality."""
        l1 = LinkedCell(5)
        assert l1.value == 5
        assert l1.next == -1
        l2 = LinkedCell(6, l1)
        assert l2.value == 6
        assert l2.next == l1
        assert l2.next.value == 5
        assert l2.next.next == -1

    def test_prepend(self):
        """Test prepend() functionality."""
        ll = LinkedCell(5)
        ll = ll.prepend(6)
        ll = ll.prepend(12)
        ll = ll.prepend(76)
        ll = ll.prepend(1)
        assert ll.value == 1
        assert ll.next.value == 76
        assert ll.next.next.value == 12
        assert ll.next.next.next.next.next == -1

    def test_seek(self):
        """Test seek() functionality."""
        ll = LinkedCell(5)
        ll = ll.prepend(6)
        ll = ll.prepend(12)
        ll = ll.prepend(76)
        ll = ll.prepend(1)
        assert ll.seek(0) == 1
        assert ll.seek(1) == 76
        assert ll.seek(4) == 5
        assert ll.seek(-1) == -1
        assert ll.seek(101) == -1
        assert ll.seek(5) == -1

    def test_len(self):
        """Test len() functionality."""
        ll = LinkedCell(5)
        ll = ll.prepend(6)
        ll = ll.prepend(12)
        ll = ll.prepend(76)
        ll = ll.prepend(1)
        assert ll.len() == 5
        assert LinkedCell(9).len() == 1

    def test_assign(self):
        """Test prepend() functionality."""
        ll = LinkedCell(1)
        ll = ll.append(2)
        ll = ll.append(3)
        ll = ll.append(4)
        ll = ll.append(5)
        assert ll.value == 1
        assert ll.next.value == 2
        assert ll.next.next.value == 3
        assert ll.next.next.next.next.next == -1

    def test_append(self):
        """Test append() functionality."""
        ll = LinkedCell(1)
        ll = ll.append(2)
        ll = ll.append(3)
        ll = ll.append(4)
        ll = ll.append(5)
        ll.assign(63, 1)
        ll.assign(68, 2)
        assert ll.value == 1
        assert ll.next.value == 63
        assert ll.next.next.value == 68
        assert ll.next.next.next.next.next == -1

    def test_insert(self):
        """Test insert() functionality."""
        ll = LinkedCell(5)
        ll = ll.prepend(6)
        ll = ll.prepend(12)
        ll = ll.prepend(76)
        ll = ll.prepend(1)
        ll.insert(7, 2)
        assert ll.value == 1
        assert ll.next.value == 76
        assert ll.next.next.value == 7
        assert ll.next.next.next.value == 12
        assert ll.next.next.next.next.next.next == -1

    def test_remove(self):
        """Test insert() functionality."""
        ll = LinkedCell(5)
        ll = ll.prepend(6)
        ll = ll.prepend(12)
        ll = ll.prepend(76)
        ll = ll.prepend(1)
        ll.remove(2)
        assert ll.value == 1
        assert ll.next.value == 76
        assert ll.next.next.value == 6
        assert ll.next.next.next.next == -1


def main():
    """Try printing functionality."""
    ll = LinkedCell(5)
    ll = ll.prepend(6)
    ll = ll.prepend(12)
    ll = ll.prepend(76)
    ll = ll.prepend(1)
    ll.simple_traverse()
    ll.remove(0)
    ll.simple_traverse()


main()
