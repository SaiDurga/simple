"""Implement doubly-linked list in Python."""

import numpy as np
# %%


class Node:
    """Node implementation for doubly-linked list."""

    def __init__(self, v=None, next=None, prev=None):
        """Construct a node."""
        self.value = v
        self.next = next
        self.prev = prev


class DoublyLinkedList:
    """Implementation of doubly-linked list."""

    def __init__(self, head=None, tail=None, length=0):
        """Construct a doubly-linked list."""
        self.head = head
        self.tail = tail
        self.length = length

    def is_empty(self):
        """Return True if the list is empty; False otherwise."""
        return self.length == 0

    def prepend(self, v):
        """Prepend given value to the linked list."""
        new_cell = Node(v, self.head, None)

        # prepend when the list is empty
        if self.is_empty():
            self.head = new_cell
            self.tail = new_cell
            self.length = 1
            return self

        # prepend when the list is not empty
        self.head.prev = new_cell
        self.head = new_cell
        self.length = self.length + 1
        return self

    def seek(self, i):
        """Return value at index i from the end of the list."""
        n = self.head
        if i < 0 or i > self.length:
            return -1
        while i > 0:
            i = i-1
            n = n.next
        return n.value

    def seek_from_end(self, i):
        """Return value at index i from the end of the list."""
        n = self.tail
        if i < 0 or i > self.length:
            return -1
        while i > 0:
            i = i-1
            n = n.prev
        return n.value

    def append(self, v):
        """Append given value to the linked list."""
        new_cell = Node(v, None, self.tail)
        if self.is_empty():
            self.head = new_cell
            self.tail = new_cell
            self.length = 1
            return self
        self.tail.next = new_cell
        self.tail = new_cell
        self.length = self.length + 1
        return self

    def assign(self, i, num):
        """Assign the given value at the given index."""
        n = self.head
        if self.is_empty() or i >= self.length or i < 0:
            return None
        if i == 0:
            n.value = num
        else:
            while i > 0:
                i = i-1
                n = n.next
            n.value = num

    def insert(self, ind, num):
        """Insert the given number at the given index."""
        n = self.head
        new_cell = Node(num)
        i = ind - 1
        if ind < 0 or ind > self.length:
            return None
        if ind == 0:
            self.prepend(num)
        elif ind == self.length:
            self.append(num)
        else:
            while i > 0:
                i = i - 1
                n = n.next
            new_cell.prev = n.next.prev
            new_cell.next = n.next
            n.next.prev = new_cell
            n.next = new_cell
            self.length = self.length + 1
            return self

    def remove(self, ind):
        """Remove the number at the given index."""
        n = self.head
        i = ind - 1
        if ind < 0 or ind > self.length:
            return None
        if ind == 0:
            n.next.prev = None
            self.head = n.next
        else:
            while i > 0:
                i = i - 1
                n = n.next
            if n.next.next is not None:
                n.next.next.prev = n.next.prev
                n.next = n.next.next
            else:
                n.next = None
                self.tail = n

    def assign_from_end(self, i, num):
        """Assign given number at the given index from the end."""
        n = self.tail
        if self.is_empty() or i >= self.length or i < 0:
            return None
        if i == 0:
            n.value = num
        else:
            while i > 0:
                i = i-1
                n = n.prev
            n.value = num

    def create_list(self, i1, i2):
        """Create a list with the given range."""
        for i in range(i1, i2+1):
            self.append(i)

    def slice_list(self, i1, i2):
        """Slice the given list."""
        n = self.head
        new_list = DoublyLinkedList()
        i = 1
        if self.is_empty():
            return None
        elif i1 < 0 or i2 < 0 or i1 > i2:
            return None
        else:
            while i <= self.length:
                if i >= i1 and i <= i2:
                    new_list.append(n.value)
                i = i+1
                n = n.next
            return new_list

    def random_select(self, num):
        """Extract numbers."""
        n = self.head
        numbers = np.random.choice(range(1, self.length+1), num, replace=False)
        new_list = DoublyLinkedList()
        i = 1
        if self.is_empty():
            return None
        while i <= self.length:
            if i in numbers:
                new_list.append(n.value)
            i = i+1
            n = n.next
        return new_list

    def simple_traverse(self):
        """Traverse and print only node values."""
        n = self.head
        print('Head ->',  end='')
        while n is not None:
            print('{} -> '.format(n.value), end='')
            n = n.next
        print('End')

    def simple_traverse_from_end(self):
        """Traverse from end."""
        n = self.tail
        print('Tail ->', end='')
        while n is not None:
            print('{} -> '.format(n.value), end='')
            n = n.prev
        print('Head')


class TestDoublyLinkedList(object):
    """Test doubly-linked list."""

    def test_creation(self):
        """Test __init()__ functionality."""
        n = Node(5)
        la = DoublyLinkedList(n, length=1)
        assert la.length == 1
        la.prepend(6)
        la.prepend(19)
        la.prepend(121)
        assert la.length == 4
        la2 = DoublyLinkedList()
        assert la2 is not None
        assert la2.length == 0
        assert la2.head is None
        assert la2.tail is None
        la2.prepend(8)
        assert la2.head is not None
        assert la2.tail is not None
        assert la2.length == 1
        assert la2.head.value == 8
        assert la2.tail.value == 8
        assert la2.head.next is None
        assert la2.tail.next is None
        assert la2.head.prev is None
        assert la2.tail.prev is None
        la2.prepend(9)
        assert la2.length == 2
        assert la2.head.value == 9
        assert la2.tail.value == 8
        assert la2.head.next is not None
        assert la2.tail.next is None
        assert la2.head.prev is None
        assert la2.tail.prev is not None
        assert la2.head.next.value == 8
        assert la2.tail.prev.value == 9
        la2.prepend(10)
        la2.prepend(11)
        la2.prepend(12)
        assert la2.seek_from_end(0) == 8
        assert la2.seek_from_end(1) == 9
        assert la2.seek_from_end(2) == 10
        assert la2.seek_from_end(3) == 11
        assert la2.seek_from_end(4) == 12

    def test_append(self):
        """Test append functionality."""
        la = DoublyLinkedList()
        assert la is not None
        assert la.length == 0
        assert la.head is None
        assert la.tail is None
        la.append(1)
        assert la.head is not None
        assert la.tail is not None
        assert la.length == 1
        assert la.head.value == 1
        assert la.head.prev is None
        assert la.head.next is None
        assert la.tail.value == 1
        assert la.tail.prev is None
        assert la.tail.next is None
        la.append(2)
        assert la.head is not None
        assert la.tail is not None
        assert la.length == 2
        assert la.head.value == 1
        assert la.head.prev is None
        assert la.head.next is not None
        assert la.tail.value == 2
        assert la.tail.prev is not None
        assert la.tail.next is None
        la.append(3)
        assert la.length == 3
        assert la.head.value == 1
        assert la.tail.value == 3
        assert la.head.next.value == 2
        assert la.tail.prev.value == 2
        assert la.head.prev is None
        assert la.head.next is not None
        assert la.head.next.prev is not None
        assert la.head.next.next is not None
        assert la.tail.prev is not None
        assert la.tail.next is None
        assert la.tail.prev.prev is not None
        assert la.tail.prev.next is not None

    def test_assign(self):
        """Test assign functionality."""
        la = DoublyLinkedList()
        assert la.assign(1, 5) is None
        la.append(1)
        la.append(2)
        la.append(3)
        la.append(4)
        la.append(5)
        assert la.head.value == 1
        assert la.head.next.value == 2
        assert la.head.next.next.value == 3
        assert la.head.next.next.next.value == 4
        assert la.head.next.next.next.next.value == 5
        assert la.length == 5
        la.assign(0, 68)
        assert la.head.value == 68
        assert la.head.next.value == 2
        assert la.head.next.next.value == 3
        assert la.head.next.next.next.value == 4
        assert la.head.next.next.next.next.value == 5
        la.assign(2, 63)
        assert la.head.value == 68
        assert la.head.next.value == 2
        assert la.head.next.next.value == 63
        assert la.head.next.next.next.value == 4
        assert la.head.next.next.next.next.value == 5
        la.assign(4, 6)
        assert la.head.value == 68
        assert la.head.next.value == 2
        assert la.head.next.next.value == 63
        assert la.head.next.next.next.value == 4
        assert la.head.next.next.next.next.value == 6
        assert la.assign(10, 74) is None
        assert la.assign(-1, 32) is None

    def test_insert(self):
        """Test insert functionality."""
        la = DoublyLinkedList()
        la.append(1)
        la.append(2)
        la.append(3)
        la.append(4)
        la.append(5)
        assert la.head.value == 1
        assert la.head.next.value == 2
        assert la.head.next.next.value == 3
        assert la.head.next.next.next.value == 4
        assert la.head.next.next.next.next.value == 5
        la.insert(3, 68)
        assert la.head.value == 1
        assert la.head.next.value == 2
        assert la.head.next.next.value == 3
        assert la.head.next.next.next.value == 68
        assert la.head.next.next.next.next.value == 4
        assert la.head.next.next.next.next.next.value == 5
        assert la.tail.value == 5
        assert la.tail.prev.value == 4
        assert la.tail.prev.prev.value == 68
        assert la.tail.prev.prev.prev.value == 3
        assert la.tail.prev.prev.prev.prev.value == 2
        assert la.tail.prev.prev.prev.prev.prev.value == 1
        la.insert(0, 10)
        assert la.head.value == 10
        la.insert(7, 17)
        assert la.head.next.next.next.next.next.next.next.value == 17

    def test_remove(self):
        """Test remove functionality."""
        la = DoublyLinkedList()
        la.append(1)
        la.append(2)
        la.append(3)
        la.append(4)
        la.append(5)
        assert la.head.value == 1
        assert la.head.next.value == 2
        assert la.head.next.next.value == 3
        assert la.head.next.next.next.value == 4
        assert la.head.next.next.next.next.value == 5
        la.remove(1)
        assert la.head.value == 1
        assert la.head.next.value == 3
        assert la.head.next.next.value == 4
        assert la.head.next.next.next.value == 5
        assert la.tail.value == 5
        assert la.tail.prev.value == 4
        assert la.tail.prev.prev.value == 3
        assert la.tail.prev.prev.prev.value == 1
        la.remove(0)
        assert la.head.value == 3
        assert la.head.next.value == 4
        assert la.head.next.next.value == 5
        assert la.tail.value == 5
        assert la.tail.prev.value == 4
        assert la.tail.prev.prev.value == 3
        la.remove(2)
        assert la.head.value == 3
        assert la.head.next.value == 4
        assert la.remove(-1) is None
        assert la.remove(101) is None

    def test_assign_from_end(self):
        """Test asssign from end functionality."""
        la = DoublyLinkedList()
        assert la.assign(1, 5) is None
        la.append(1)
        la.append(2)
        la.append(3)
        la.append(4)
        la.append(5)
        assert la.head.value == 1
        assert la.head.next.value == 2
        assert la.head.next.next.value == 3
        assert la.head.next.next.next.value == 4
        assert la.head.next.next.next.next.value == 5
        assert la.length == 5
        la.assign_from_end(0, 10)
        assert la.head.value == 1
        assert la.head.next.value == 2
        assert la.head.next.next.value == 3
        assert la.head.next.next.next.value == 4
        assert la.head.next.next.next.next.value == 10
        la.assign_from_end(4, 68)
        assert la.head.value == 68
        la.assign_from_end(2, 63)
        assert la.head.value == 68
        assert la.head.next.value == 2
        assert la.head.next.next.value == 63
        assert la.head.next.next.next.value == 4
        assert la.head.next.next.next.next.value == 10
        assert la.assign_from_end(-1, 200) is None
        assert la.assign_from_end(100, 200) is None

    def test_create_list(self):
        """Test createlist functionality."""
        la = DoublyLinkedList()
        la.create_list(1, 5)
        assert la.head.value == 1
        assert la.head.next.value == 2
        assert la.head.next.next.value == 3
        assert la.head.next.next.next.value == 4
        assert la.head.next.next.next.next.value == 5
        assert la.tail.value == 5
        assert la.tail.prev.value == 4
        assert la.tail.prev.prev.value == 3
        assert la.tail.prev.prev.prev.value == 2
        assert la.tail.prev.prev.prev.prev.value == 1
        assert la.length == 5

    def test_slice_list(self):
        """Test slice_list functionality."""
        la = DoublyLinkedList()
        assert la.slice_list(1, 3) is None
        la.append(1)
        la.append(2)
        la.append(3)
        la.append(4)
        la.append(5)
        assert la.length == 5
        ls = la.slice_list(1, 3)
        assert ls.head.value == 1
        assert ls.head.next.value == 2
        assert ls.head.next.next.value == 3
        assert ls.tail.value == 3
        assert ls.tail.prev.value == 2
        assert ls.tail.prev.prev.value == 1
        assert ls.length == 3
        assert la.slice_list(-1, 2) is None
        assert la.slice_list(1, -2) is None
        assert la.slice_list(1, 1).head.value == 1
        assert la.slice_list(1, 1).tail.value == 1

# %%


def main():
    """Try printing functionality."""
    la = DoublyLinkedList()
    la = la.prepend(5)
    la = la.prepend(4)
    la = la.prepend(3)
    la = la.prepend(2)
    la.simple_traverse()
    la.random_select(2).simple_traverse()


main()
# %%


def main2():
    """Try printing functionality."""
    la = DoublyLinkedList()
    la = la.prepend(5)
    la = la.prepend(4)
    la = la.prepend(3)
    la = la.prepend(2)
    la.simple_traverse_from_end()
    la.assign_from_end(0, 1)
    la.simple_traverse_from_end()


main2()
