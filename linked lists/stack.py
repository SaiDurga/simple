"""Implement Stack."""


class Stack():
    """Implement stack."""

    def __init__(self):
        """Stack."""
        self.list = []

    def push(self, ele):
        """Push function."""
        self.list.append(ele)

    def pop(self):
        """Pop function."""
        return self.list.pop()

    def top(self):
        """Get the top element."""
        return self.list[-1]


class TestStack(object):
    """Test class for stack."""

    def test_push(self):
        """Test push() functionality."""
        s = Stack()
        s.push(1)
        s.push(2)
        assert s.list == [1, 2]
        s.push(3)
        s.push(4)
        s.push(5)
        assert s.list == [1, 2, 3, 4, 5]

    def test_pop(self):
        """Test pop() functionality."""
        s = Stack()
        s.push(1)
        s.push(2)
        assert s.list == [1, 2]
        s.push(3)
        s.push(4)
        s.push(5)
        assert s.list == [1, 2, 3, 4, 5]
        s.pop()
        assert s.list == [1, 2, 3, 4]
        s.pop()
        assert s.list == [1, 2, 3]

    def test_top(self):
        """Test push() functionality."""
        s = Stack()
        s.push(1)
        s.push(2)
        s.push(3)
        s.push(4)
        s.push(5)
        assert s.list == [1, 2, 3, 4, 5]
        assert s.top() == 5
        s.pop()
        assert s.top() == 4
