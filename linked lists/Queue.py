"""Implement Queue."""

import linked_list as l_l


class Queue():
    """Implement queue."""

    def __init__(self, ele):
        """Queue."""
        self.list = l_l.LinkedCell(ele)

    def push(self, ele):
        """Implement push in queue."""
        self.list.append(ele)

    def top(self):
        """Implement top in queue."""
        return self.list.seek(0)

    def pop(self):
        """Implement pop in queue."""
        p_e = self.list.seek(0)
        self.list.remove(0)
        self.list.length = self.list.length - 1
        return p_e


class TestStack(object):
    """Test class for queue."""

    def test_push(self):
        """Test push() functionality."""
        s = Queue(1)
        s.push(2)
        s.push(3)
        s.push(4)
        s.push(5)
        assert s.list.value == 1
        assert s.list.next.value == 2
        assert s.list.next.next.next.next.value == 5

    def test_top(self):
        """Test top() functionality."""
        s = Queue(1)
        s.push(2)
        s.push(3)
        s.push(4)
        s.push(5)
        assert s.top() == 1

    def test_pop(self):
        """Test pop() functionality."""
        s = Queue(1)
        s.push(2)
        s.push(3)
        s.push(4)
        s.push(5)
        s.pop()
        assert s.top() == 2
        s.pop()
        assert s.top() == 3
