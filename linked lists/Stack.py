"""Implement Stack."""

import linked_list as l_l


class Stack():
    """Implement stack."""

    def __init__(self, ele):
        """Stack."""
        self.list = l_l.LinkedCell(ele)

    def push(self, ele):
        """Implement push in stack."""
        self.list.append(ele)

    def top(self):
        """Implement top in stack."""
        id = self.list.length - 1
        return self.list.seek(id)

    def pop(self):
        """Implement pop in stack."""
        id = self.list.length - 1
        p_e = self.list.seek(id)
        self.list.remove(id)
        self.list.length = self.list.length - 1
        return p_e


class TestStack(object):
    """Test class for stack."""

    def test_push(self):
        """Test push() functionality."""
        s = Stack(1)
        s.push(2)
        s.push(3)
        s.push(4)
        s.push(5)
        assert s.list.value == 1
        assert s.list.next.value == 2
        assert s.list.next.next.next.next.value == 5

    def test_top(self):
        """Test top() functionality."""
        s = Stack(1)
        s.push(2)
        s.push(3)
        s.push(4)
        s.push(5)
        assert s.top() == 5

    def test_pop(self):
        """Test pop() functionality."""
        s = Stack(1)
        s.push(2)
        s.push(3)
        s.push(4)
        s.push(5)
        s.pop()
        assert s.top() == 4
        s.pop()
        assert s.top() == 3
