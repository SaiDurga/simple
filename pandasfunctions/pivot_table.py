"""Practice pivot table."""

# %%
# import statements
import pandas as pd
import seaborn as sns

# %%
# loading dataset
titanic = sns.load_dataset('titanic')
titanic.head()

# %%
# surival rate -- groupby
titanic.groupby('sex')[['survived']].mean()
titanic.groupby(['sex', 'class'])['survived'].aggregate('mean').unstack()

# %%
# use pivot table for the same above
titanic.pivot_table('survived', index='sex', columns='class')

# %%
# multilevel pivot table
age = pd.cut(titanic['age'], [0, 18, 80])
titanic.pivot_table('survived', ['sex', age], 'class')

fare = pd.qcut(titanic['fare'], 2)
titanic.pivot_table('survived', ['sex', age], [fare, 'class'])

# %%
# change aggregate function, default is mean
titanic.pivot_table(index='sex', columns='class',
aggfunc={'survived':sum, 'fare':'mean'})

# %%
# to compute totals along each grouping, use margins. Set margins=True
titanic.pivot_table('survived', index='sex', columns='class', margins=True)
