"""Practice crosstab in pandas."""

import numpy as np
import pandas as pd

a = (np.array(["foo", "foo", "foo", "foo", "bar", "bar", "bar", "bar", "foo",
     "foo", "foo"]))

b = (np.array(["one", "one", "one", "two", "one", "one", "one", "two", "two",
     "two", "one"]))

c = (np.array(["dull", "dull", "shiny", "dull", "dull", "shiny", "shiny",
     "dull", "shiny", "shiny", "shiny"]))

b
c

pd.crosstab(a, [b, c], rownames=['a'], colnames=['b', 'c'])

# %%

foo = pd.Categorical(['a', 'b'], categories=['a', 'b', 'c'])
foo1 = pd.Categorical(['a', 'b'], categories=['a', 'b'])
foo
foo1

bar = pd.Categorical(['d', 'e'], categories=['d', 'e', 'f'])
bar

pd.crosstab(foo, bar)
pd.crosstab(foo1, bar)
foo
