"""practice join in pandas."""

import pandas as pd

# %%
caller = (pd.DataFrame({'key': ['K0', 'K1', 'K2', 'K3', 'K4', 'K5'],
           'A': ['A0', 'A1', 'A2', 'A3', 'A4', 'A5']}))
caller

other =   pd.DataFrame({'key': ['K0', 'K1', 'K2'], 'B': ['B0', 'B1', 'B2']})
other

c1 = caller.join(other,lsuffix='_key1', rsuffix='_key2')
c1

# %%
# to join using key column set key column as index
caller.set_index('key').join(other.set_index('key'))

# %%
# to join using a column in caller instead of set_index
caller.join(other.set_index('key'), on='key')

# %%
# check merge and concat
pd.merge(caller, other)
pd.merge(caller, other, how='outer')
pd.concat([caller, other])


# %%
# change index of caller
index = [6, 7, 8, 9, 10, 11]
caller1 = (pd.DataFrame({'key': ['K0', 'K1', 'K2', 'K3', 'K4', 'K5'],
           'A': ['A0', 'A1', 'A2', 'A3', 'A4', 'A5']}, index = index))
caller1
caller1.join(other, lsuffix='_caller1', rsuffix='_other')
# after changing index join doesnt occur
# now, index of both the datframes are changed to key and then joined
caller1.set_index('key').join(other.set_index('key'))

# %%
# check merge and concat
pd.merge(caller1, other)
pd.merge(caller1, other, how='outer')
pd.concat([caller1, other], axis=1)
