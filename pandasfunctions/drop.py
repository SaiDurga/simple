"""Practice drop in pandas."""

import numpy as np
import pandas as pd

# %%
# create a dataframe
df = pd.DataFrame(np.arange(12).reshape(3, 4), columns=['A', 'B', 'C', 'D'])
df

# %%
# drop columns
df.drop(['B', 'C'], axis=1)

# %%
df.drop(columns=['B', 'C'])

# %%
df.drop(columns=['B', 'C'], axis=1)

# %%
# drop rows
df.drop([0, 1])

# %%
# errors
df.drop([0, 1], columns=['B', 'C'])
df.drop(['A', 'B'])

# %%
# create another dataframe
df = (pd.DataFrame(np.arange(25).reshape(5, 5),
      columns=['A', 'B', 'C', 'D', 'E']))
df
df.drop(['A'], axis=1)
df.drop(columns=['C'])
df.drop([0])
df.drop([1, 2, 3])
