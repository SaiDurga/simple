"""Practice pandas function merge."""

# %%
# import statements
import pandas as pd

# %%
# create dataframes
df1 = (pd.DataFrame({'employee': ['Bob', 'Jake', 'Lisa', 'Sue'],
       'group': ['Accounting', 'Engineering', 'Engineering', 'HR']}))
df2 = (pd.DataFrame({'employee': ['Lisa', 'Bob', 'Jake', 'Sue'],
       'hire_date': [2004, 2008, 2012, 2014]}))
df1
df2

# %%
# One-to-one join
df3 = pd.merge(df1, df2)
df3_1 = pd.merge(df2, df1)
df3_1
# df3 and df3_1 are different

# %%
# many to one joins
df4 = (pd.DataFrame({'group': ['Accounting', 'Engineering', 'HR'],
       'supervisor': ['Carly', 'Guido', 'Steve']}))
df4

pd.merge(df3, df4)

# %%
# Many-to-many joins
df5 = (pd.DataFrame({'group': ['Accounting', 'Accounting', 'Engineering',
       'Engineering', 'HR', 'HR'], 'skills': ['math', 'spreadsheets', 'coding',
       'linux', 'spreadsheets', 'organization']}))
df5
pd.merge(df1, df5)
pd.merge(df5, df1)

# %%
# Specification of the Merge Key
pd.merge(df1, df2, on='employee')
pd.merge(df1, df2)

# %%
# The left_on and right_on keywords
df3a = (pd.DataFrame({'name': ['Bob', 'Jake', 'Lisa', 'Sue'],
        'salary': [70000, 80000, 120000, 90000]}))
df3a
pd.merge(df1, df3a, left_on="employee", right_on="name")

# %%
# The left_index and right_index keywords
df1a = df1.set_index('employee')
df2a = df2.set_index('employee')
pd.merge(df1a, df2a, left_index=True, right_index=True)
pd.merge(df1a, df3a, left_index=True, right_on='name')
