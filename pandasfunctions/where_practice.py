"""practice panadas.DataFrame.where."""

import numpy as np
import pandas as pd

# %%
# create a series
s = pd.Series(range(5))
s

# %%
# replaces the entries with Nan where the condition is false.
s.where(s>0)

# %%
# if value gven, replaces the entry with the given value when condition is false
s.where(s>0, 10)

# %%
# create a DataFrame
df = pd.DataFrame(np.arange(10), columns = ['A'])
df

# %%
#specify condition- if no value given replaces with nan
df.where(df % 3 == 0)

# %%
# replace with given value when condition is false
df.where(df % 3 == 0, -df)

# %%
# using np.where we can sepecify two values for condition and true false
np.where(df % 3 == 0, df, -df)
np.where(df % 2 == 0, 'even', 'odd')

# %%
# can specify in either the way shown below.
df.where(df % 3 == 0, -df) == np.where(df % 3 == 0, df, -df)

# %%
# mask--opposite to where--replaces when condition is false
df.mask(df % 2 == 0, 'odd')
