"""Practice map in pandas."""

import pandas as pd
import numpy as np

# %%
x = pd.Series([1, 2, 3], index=['one', 'two', 'three'])
y = pd.Series(['foo', 'bar', 'baz'], index=[1, 2, 3])
x
y

# %%
x.map(y)
x
y.map(x)
y

# %%
# change the index of x
x1 = pd.Series(['a', 'b', 'c'], index=['foo', 'bar', 'baz'])
x1
x1.map(y)
y.map(x1)

# %%
z = {1: 'A', 2: 'B', 3: 'C'}
x.map(z)

# %%
# Use na_action to control the effect of map on Nan values.
s = pd.Series([1, 2, 3, np.nan])
s

s2 = s.map('this is a string {}'.format, na_action=None)
s2

s3 = s.map('this is a string {}'.format, na_action='ignore')
s3

# %%
# function map


def squares(x):
    """Find square of a number."""
    return x * x


s4 = s.map(squares)
s4
s5 = s.map(squares, na_action='ignore')
s5

# %%
# vary lengths of mappingseries
m = pd.Series([0, 1, 2, 3, 4, 5])
m
n = pd.Series(['a', 'b', 'c', 'd', 'e'], index=[1, 2, 3, 4, 5])
n
m.map(n)
o = pd.Series(['a', 'b', 'c', 'd', 'e'], index=[7, 2, 3, 9, 5])
o
m.map(o)

# %%
p = pd.Series(['a', 'b', 'c', 'd', 'e', 'f', 'g'], index=[1, 2, 3, 4, 5, 6, 7])
p
m.map(p)
