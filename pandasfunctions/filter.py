"""Practice filter in pandas."""

import pandas as pd

df = pd.DataFrame({'one': [1, 4], 'two': [2, 5], 'three': [3, 6]},
                  index=['mouse', 'rabbit'])
df

# %%
# select columns by name
df.filter(items=['one', 'two'])

# %%
df.filter(items=['one'])

# %%
# select columns by regular expressions
df.filter(regex='e$')

# %%
df.filter(regex='e$', axis=1)

# %%
df.filter(regex='e$', axis=0)

# %%
df.filter(regex='^t')

# %%
df.filter(regex='^t', axis=0)

# %%
# select rows containing 'bbi'
df.filter(like='bbi', axis=0)

# %%
df.filter(like='bbi', axis=1)

# %%
df.filter(like='ou', axis=0)

# %%
df.filter(like='ee', axis=1)
