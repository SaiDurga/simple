"""Practice sort in pandas."""

import pandas as pd

# %%
# create a dataframe
d = (pd.DataFrame({'one': [2, 3, 1, 4, 5],
     'two': [5, 4, 3, 2, 1],
     'letter': ['a', 'a', 'b', 'b', 'c']}))

# %%
# sort- by default it sorts in ascending order
d.sort_values(by='one')

# %%
# to have sorting in descending order set ascending to False
d.sort_values(by='one', ascending=False)

# %%
# set by to the column name based on which we would like to have sorting
d.sort_values(by='two')

# %%
# create a series
s1 = pd.Series([5, 7, 2, 8, 1, 4, 3])
s1

# %%
# sorting in series doesnt require to mention 'by'
s2 = s1.sort_values()
s2

# %%
# sorting can also be done based on the index using sort_index
s2.sort_index()
d1 = d.sort_values(by='one', ascending=False)
d1
d1.sort_index()
