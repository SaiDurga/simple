"""Practice split in pandas."""

import pandas as pd
import numpy as np

# %%
# create a series
s = pd.Series(["this is good text", "but this is even better"])
s

# %%
# split-If string or reg exp is not specified to split on, then splits on
# whitespace
s.str.split()
s.str.split(',')
s.str.split("random")

# %%
# another series
s1 = pd.Series(["this/is/good/text"])
s1.str.split('/')

# %%
# When using expand=True, the split elements will expand out into separate
# columns.
s.str.split(expand=True)
s.str.split("is", expand=True)
s1.str.split("/", expand=True)

# %%
# for index object, output return type is multi reset_index
i = pd.Index(["a b c", "d e f", "g h i"])
i
i.str.split()
i.str.split(expand=True)
i.str.split(',')
i.str.split(',', expand=True)

# %%
# no of splits can be limited by using the parameter n
s.str.split(n=1)
s1.str.split('/', n=1)
s1.str.split('/', n=1, expand=True)

# %%
# If there is Nan, it is propagated throughout the columns during the split
s2 = pd.Series(["this is good text", "but this is even better", np.nan])
s2
s2.str.split()
s2.str.split(n=3, expand=True)
