"""Practice rank in pandas."""

import pandas as pd


# %%
# create a datframe
df1 = ({'Name': ['Alisa', 'Bobby', 'Cathrine', 'Alisa', 'Bobby', 'Cathrine',
                 'Alisa', 'Bobby', 'Cathrine', 'Alisa', 'Bobby', 'Cathrine'],
       'Subject': ['Mathematics', 'Mathematics', 'Mathematics', 'Science',
                   'Science', 'Science', 'History', 'History', 'History',
                   'Economics', 'Economics', 'Economics'],
        'Score': [62, 47, 55, 74, 31, 77, 85, 63, 42, 62, 89, 85]})

df2 = pd.DataFrame(df1, columns=['Name', 'Subject', 'Score'])
df2

# %%
df3 = df2
df3['score_ranked'] = df3['Score'].rank(ascending=1)
df3

# %%
# use 'min' method to find the rank
df3['score_ranked_min'] = df3['Score'].rank(ascending=1, method='min')
df3

# %%
# use 'max' method to find the rank
df3['score_ranked_max'] = df3['Score'].rank(ascending=1, method='max')
df3

# %%
# use 'average' method to find the rank
df3['score_ranked_avg'] = df3['Score'].rank(ascending=1, method='average')
df3

# %%
# use 'first' method to find the rank
df3['score_ranked_first'] = df3['Score'].rank(ascending=1, method='first')
df3

# %%
# use 'dense' method to find the rank
df3['score_ranked_dense'] = df3['Score'].rank(ascending=1, method='dense')
df3


# %%
# create another datframe
data = {'name': ['Jason', 'Molly', 'Tina', 'Jake', 'Amy'],
        'year': [2012, 2012, 2013, 2014, 2014],
        'reports': [4, 24, 31, 2, 3],
        'coverage': [25, 94, 62, 62, 70]}
d = (pd.DataFrame(data, index=['Cochice', 'Pima', 'Santa Cruz', 'Maricopa',
     'Yuma']))
d

# %%
# use 'min' method to find the rank
d1 = d
d1['Rank_min'] = d1['coverage'].rank(ascending=1, method='min')
d1

# %%
# use 'max' method to find the rank
d1['Rank_max'] = d1['coverage'].rank(ascending=1, method='max')
d1

# %%
# use 'average' method to find the rank
d1['Rank_avg'] = d1['coverage'].rank(ascending=1, method='average')
d1

# %%
# use 'first' method to find the rank
d1['Rank_first'] = d1['coverage'].rank(ascending=1, method='first')
d1

# %%
# use 'dense' method to find the rank
d1['Rank_dense'] = d1['coverage'].rank(ascending=1, method='dense')
d1
