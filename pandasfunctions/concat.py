"""Practice concat."""

import numpy as np
import pandas as pd

x = [1, 2, 3]
y = [4, 5, 6]
z = [7, 8, 9]
np.concatenate([x, y, z])

s1 = pd.Series(['a', 'b'])
s2 = pd.Series(['c', 'd'])
pd.concat([s1, s2])

pd.concat([s1, s2], ignore_index=True)

pd.merge(s1, s2)

pd.concat([s1, s2], keys=['s1', 's2'])
pd.concat([s1, s2], keys=['s1', 's2'], names=['Series name', 'Row ID'])

# %%
# DataFrames
df1 = pd.DataFrame([['a', 1], ['b', 2]], columns=['letter', 'number'])
df2 = pd.DataFrame([['c', 3], ['d', 4]], columns=['letter', 'number'])
pd.concat([df1, df2])
# pd.merge(df1, df2, left_index=True, right_index=True)
# %%
df3 = (pd.DataFrame([['c', 3, 'cat'], ['d', 4, 'dog']],
       columns=['letter', 'number', 'animal']))
df3
pd.concat([df1, df3], join='outer')
pd.concat([df1, df3], join='inner')
pd.merge(df1, df2, left_index=True, right_index=True)

# %%
df4 = (pd.DataFrame([['bird', 'polly'], ['monkey', 'george']],
       columns=['letter', 'name']))
pd.concat([df1, df4])
pd.concat([df1, df4])
df1
df4
pd.merge(df1, df4, left_index=True, right_index=True)

# %%
p1 = pd.Series([1, 2])
p2 = pd.Series([1, 2], index=[3, 4])
pd.concat([p1, p2])

# %%
d1 = pd.DataFrame([['a', 1], ['b', 2], ['e', 5]], columns=['letter1', 'number'])
d2 = pd.DataFrame([['c', 3], ['d', 4], ['e', 5]], columns=['letter1', 'number1'])
d1
d2
pd.concat([d1, d2], axis=1)
pd.merge(d1, d2)
