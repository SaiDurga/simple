"""Practice melt in pandas."""

import pandas as pd

# %%
# create a dataframe
df = (pd.DataFrame({'A': {0: 'a', 1: 'b', 2: 'c'}, 'B': {0: 1, 1: 3, 2: 5},
      'C': {0: 2, 1: 4, 2: 6}}))
df

# %%
pd.melt(df, id_vars=['A'], value_vars=['B'])
pd.melt(df, id_vars=['B'], value_vars=['A'])

# %%
pd.melt(df, id_vars=['A'], value_vars=['B', 'C'])

# %%
pd.melt(df, value_vars=['A', 'B', 'C'])

# %%
# The names of ‘variable’ and ‘value’ columns can be changed.
(pd.melt(df, id_vars=['A'], value_vars=['B'],
 var_name='myVarname', value_name='myValname'))

# %%
# multi-index columns
df.columns = [list('ABC'), list('DEF')]
df

# %%
pd.melt(df, col_level=0, id_vars=['A'], value_vars=['B'])
# pd.melt(df, col_level=0, id_vars=['D'], value_vars=['E'])

# %%
pd.melt(df, id_vars=[('A', 'D')], value_vars=[('B', 'E')])
(pd.melt(df, id_vars=[('A', 'D')], value_vars=[('C', 'F')],
 var_name=['var1', 'var2'], value_name='val'))
