"""Practice cumsum and cumcount functions in pandas."""

import pandas as pd
import numpy as np

# %%
# create a series
s = pd.Series([1, 2, 3, 4])

# %%
s.cumsum()

# %%
# create another series with some nans
s1 = pd.Series([2, 5, np.nan, 6])
s1

# %%
s1.cumsum()

# %%
# default values for skipna is True
s1.cumsum(skipna=False)

# %%
# create a dataframe
df = pd.DataFrame([[2.0, 1.0], [3.0, 4.0], [1.0, 0.0]], columns=list('AB'))
df

# %%
df.cumsum()

# %%
df.cumsum(axis=1)

# %%
# another dataframe
df1 = pd.DataFrame([[2.0, 1.0], [3.0, np.nan], [1.0, 0.0]], columns=list('AB'))
df1

# %%
df1.cumsum()

# %%
df1.cumsum(skipna=False)

# %%
df1.cumsum(axis=1)

# %%
df1.cumsum(axis=1, skipna=False)

# %%
# CUMCOUNT
# create a dataframe
df2 = pd.DataFrame([['a'], ['a'], ['a'], ['b'], ['b'], ['a']], columns=['A'])
df2

# %%
df2.groupby('A').cumcount()

# %%
df2.groupby('A').cumcount(ascending=False)
