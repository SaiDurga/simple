"""Practice diff in pandas."""

import pandas as pd

# %%
# create a dataframe
df = (pd.DataFrame({'a': [1, 2, 3, 4, 5, 6], 'b': [1, 1, 2, 3, 5, 8],
      'c': [1, 4, 9, 16, 25, 36]}))
df

# %%
# default axis is 0
df.diff()

# %%
# column wise
df.diff(axis=1)

# %%
# Difference with third previous row
df.diff(periods=3)
df.diff(periods=2)

# %%
# Difference with the next row instead of previous row
df.diff(periods=-1)

# ------------------------------------------------------------------
# %%
# aggregate
df.agg(['sum'])
df.agg(['sum', 'min'])

# %%
df.agg('sum', axis="columns")

# %%
# different aggregations for each columns
df.agg({'a': ['sum'], 'b': ['mean']})
df.agg({'a': 'sum', 'b': 'mean', 'c': 'min'})
df.agg({'a': ['sum'], 'b': ['mean'], 'c': ['min']})
