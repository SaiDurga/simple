"""Implement Quick sort."""


# %%


def quick_sort(lst):
    """Implement quicksort."""
    return quick_sort_r(lst, 0, len(lst)-1)


def quick_sort_r(lst, ll, r):
    """Qiuck sort recursion."""
    if ll < r:
        i = partition1(lst, ll, r)
        quick_sort_r(lst, ll, i-1)
        quick_sort_r(lst, i+1, r)
    return lst


def partition1(lst, ll, r):
    """Implement partitiom."""
    i = ll
    j = r-1
    pe = lst[r]
    while j >= i:
        while lst[i] < pe:
            i = i+1
        while lst[j] > pe:
            j = j-1
        if i <= j:
            lst[j], lst[i] = lst[i], lst[j]
            i = i+1
            j = j-1
    lst[i], lst[r] = lst[r], lst[i]
    return i


# %%


def test_quicksort1():
    """Test case for quick sort."""
    assert quick_sort([10, 2, 3, 5, 6, 8, 7]) == [2, 3, 5, 6, 7, 8, 10]


def test_quicksort2():
    """Test case with already sorted list."""
    assert quick_sort([2, 3, 5, 6, 8, 11]) == [2, 3, 5, 6, 8, 11]


def test_quicksort3():
    """Test case with repeated elements."""
    assert quick_sort([2, 3, 5, 3, 8, 2]) == [2, 2, 3, 3, 5, 8]


def test_quicksort4():
    """Test case with one element."""
    assert quick_sort([68]) == [68]


def test_quicksort5():
    """Test case with two elements."""
    assert quick_sort([21, 19]) == [19, 21]


def test_quicksort6():
    """Test case with empty ist."""
    assert quick_sort([]) == []


def test_quicksort7():
    """Test case with negative numbers."""
    assert quick_sort([-1, 2, 6, 3, 8, -9, -12]) == [-12, -9, -1, 2, 3, 6, 8]


def test_quicksort8():
    """Test case with floats."""
    assert (quick_sort([1.3, 1.4, -2.8, 32, 32.0, 68]) ==
            [-2.8, 1.3, 1.4, 32.0, 32, 68])

def test_quicksort9():
    """Test case with two same elements."""
    assert quick_sort([2, 2]) == [2, 2]
