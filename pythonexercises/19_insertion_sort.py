"""Insertion sort."""


def insertion(lst):
    """Sort given list using insertion sort."""
    if len(lst) == 0:
        return None
    for i in range(1, len(lst)):
        j = i-1
        while j >= 0:
            if lst[j] > lst[i]:
                lst[j], lst[i] = lst[i], lst[j]
            j = j-1
            i = i-1
    return lst


def test_insertion1():
    """Test case for insertion."""
    assert insertion([10, 2, 3, 5, 6, 8, 7]) == [2, 3, 5, 6, 7, 8, 10]


def test_insertion2():
    """Test case with already sorted list."""
    assert insertion([2, 3, 5, 6, 8, 11]) == [2, 3, 5, 6, 8, 11]


def test_insertion3():
    """Test case with repeated elements."""
    assert insertion([2, 3, 5, 3, 8, 2]) == [2, 2, 3, 3, 5, 8]


def test_insertion4():
    """Test case with one element."""
    assert insertion([68]) == [68]


def test_insertion5():
    """Test case with empty ist."""
    assert insertion([]) is None
