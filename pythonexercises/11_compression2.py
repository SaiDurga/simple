"""Encode consecutive duplicates, if no duplicates copy into result."""


def sublists(lst):
    """Collect consecutive duplicates of list elements into sublists."""
    res = []
    res1 = []
    if len(lst) != 0:
        res.append(lst[0])
        for i in range(1, len(lst)):
            if lst[i] == lst[i-1]:
                res.append(lst[i])
            else:
                res1.append(res)
                return res1 + sublists(lst[i:])
        return res1 + [res]
    return None


def compression2(lst):
    """Encode consecutive duplicates of elements as lists."""
    res = sublists(lst)
    res1 = []
    if len(lst) != 0:
        for e in res:
            if len(e) == 1:
                res1.append(e[0])
            else:
                res1.append([len(e), e[0]])
        return res1
    return None


def test_compress1():
    """Test case for compression."""
    assert compression2([1, 1, 1, 2, 2, 3]) == [[3, 1], [2, 2], 3]


def test_compress2():
    """Test case for compression."""
    assert (compression2(['a', 'a', 'b', 'b', 'c']) ==
            [[2, 'a'], [2, 'b'], 'c'])


def test_compress3():
    """Test case for compression with two same numbers."""
    assert compression2([1, 1]) == [[2, 1]]


def test_compress4():
    """Test case for compression with two different numbers."""
    assert compression2([1, 2]) == [1, 2]


def test_compress5():
    """Test case for compression with single element."""
    assert compression2([1]) == [1]


def test_compress6():
    """Test case with empty list."""
    assert compression2([]) is None
