"""Finding kth elemnt in a given list."""


def kthelemnt(lst, k):
    """Find kth element in a given list."""
    if len(lst) >= k:
        if len(lst) == k:
            return lst[-1]
        return kthelemnt(lst[:-1], k)
    print("list contains elemnts less than {}".format(k))


def test_kthelement1():
    """Test case for kth element."""
    assert kthelemnt([1, 2, 3, 4, 5], 3) == 3


def test_kthelement2():
    """Test case for kth element."""
    assert kthelemnt([1.3, 3.5, 5.7, 7.8], 3) == 5.7


def test_kthelement3():
    """Test case for kth element."""
    assert kthelemnt([1, 3.5, 5.6, 67, 63, 68.9, 10.0, 32, 32, 0.1], 7) == 10.0


def test_kthelement4():
    """Test case for kth element."""
    assert kthelemnt(['a', 'b', 'c', 'd', 'e'], 3) == 'c'


def test_kthelement5():
    """Test case for kth element."""
    assert kthelemnt(['a', 1, 'b', 'c', 2, 3], 2) == 1


def test_kthelement6():
    """Test case for kth element."""
    assert kthelemnt(['a', 1, 'b', 'c', 2, 3, 'd'], 3) == 'b'


def test_kthelement7():
    """Test case for kth element."""
    assert kthelemnt([1, 3.5, 'a', 'b', 's', 2.8, 2, 'k'], 6) == 2.8


def test_kthelement8():
    """Test case for kth element."""
    assert kthelemnt([12000, 1800, 17864, 123.45], 3) == 17864


def test_kthelement9():
    """Test case for kth element."""
    assert kthelemnt(['abc', 'def', 'mno'], 2) == 'def'


def test_kthelement10():
    """Test case for kth element."""
    assert kthelemnt([1, 1.8, 18, 1800, 'a', 'abc'], 4) == 1800


def test_kthelement11():
    """Test case with empty list."""
    kthelemnt([], 3) == 'list contains elements less than 3'


def test_kthelement12():
    """Test case with elemnts less than the required element."""
    kthelemnt([1], 3) == 'list contains elements less than 3'


def test_kthelement13():
    """Test case with elemnts less than the required element."""
    kthelemnt([1, 2], 3) == 'list contains elements less than 3'


def test_kthelement14():
    """Test case with same no of elemnts and requirwed element."""
    assert kthelemnt([1, 2, 3, 4], 4) == 4
