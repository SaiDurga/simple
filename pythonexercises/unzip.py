"""Implement unzip."""

a1 = [1, 2, 3, 10]
a2 = [4, 5, 6, 20]
z = zip(a1, a2)
for i, j in z:
    print(i, j)


# %%
z2 = zip(*[(1, 4), (2, 5), (3, 6), (10, 20)])
z2
for i in z2:
    print(i)
# %%


def unzip(z):
    """Unzip the two lists."""
    a1 = []
    a2 = []
    for i, j in z:
        a1.append(i)
        a2.append(j)
    return a1, a2


uz = unzip(z)
uz
