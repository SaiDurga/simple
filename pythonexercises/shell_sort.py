"""Implement shell sort."""

import numpy as np


def insertion(lst):
    """Sort given list using insertion sort."""
    if len(lst) == 0:
        return None
    for i in range(1, len(lst)):
        j = i-1
        while j >= 0:
            if lst[j] > lst[i]:
                lst[j], lst[i] = lst[i], lst[j]
            j = j-1
            i = i-1
    return lst


# %%


def shellsort(lst, gaps=[5, 3, 1]):
    """Implement shellsort."""
    if len(lst) == 0:
        return None
    l1 = len(lst)
    for i in gaps:
        j = 0
        while j <= i-1:
            ind = np.arange(j, l1, i)
            ins = insertion(lst[j:l1:i])
            for a in range(len(ind)):
                lst[ind[a]] = ins[a]
            j = j+1
    return lst


# %%


def test_shellsort1():
    """Test case for shell sort."""
    assert shellsort([10, 2, 3, 5, 6, 8, 7]) == [2, 3, 5, 6, 7, 8, 10]


def test_shellsort2():
    """Test case with already sorted list."""
    assert shellsort([2, 3, 5, 6, 8, 11]) == [2, 3, 5, 6, 8, 11]


def test_shellsort3():
    """Test case with repeated elements."""
    assert shellsort([2, 3, 5, 3, 8, 2]) == [2, 2, 3, 3, 5, 8]


def test_shellsort4():
    """Test case with one element."""
    assert shellsort([68]) == [68]


def test_shellsort5():
    """Test case with two elements."""
    assert shellsort([21, 19]) == [19, 21]


def test_shellsort6():
    """Test case with empty ist."""
    assert shellsort([]) is None


def test_shellsort7():
    """Test case with negative numbers."""
    assert shellsort([-1, 2, 6, 3, 8, -9, -12]) == [-12, -9, -1, 2, 3, 6, 8]


def test_shellsort8():
    """Test case with floats."""
    assert (shellsort([1.3, 1.4, -2.8, 32, 32.0, 68]) ==
            [-2.8, 1.3, 1.4, 32.0, 32, 68])


def test_shellsort9():
    """Test case with two same elements."""
    assert shellsort([2, 2]) == [2, 2]
