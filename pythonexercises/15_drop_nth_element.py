"""Drop nth element in a given list."""


def dropping(lst, n):
    """Drop nth elemnt."""
    res = []
    if len(lst) != 0:
        for i in range(1, len(lst)+1):
            if i % n != 0:
                res.append(lst[i-1])
        return res
    return None


def test_drop1():
    """Test case for dropping."""
    assert dropping([1, 2, 3, 4, 5], 2) == [1, 3, 5]


def test_drop2():
    """Test case for dropping."""
    assert dropping([1, 2, 3, 4, 5], 3) == [1, 2, 4, 5]


def test_drop3():
    """Test case for dropping."""
    assert dropping([1, 2, 3], 3) == [1, 2]


def test_drop4():
    """Test case for dropping with single element."""
    assert dropping([1], 2) == [1]


def test_drop5():
    """Test case for dropping with single element."""
    assert dropping([1], 1) == []


def test_drop6():
    """Test case for dropping with empty list."""
    assert dropping([], 2) is None


def test_drop7():
    """Test case for dropping with empty list."""
    assert dropping([1, 2, 3], 0) is None
