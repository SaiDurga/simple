"""Implement merge sort."""


def merge_sort(lst):
    """Implement merge sort."""
    if len(lst) == 0:
        return None
    l1 = 0
    m = len(lst)//2
    r = len(lst)
    return merge_sort_r(lst, l1, m, r)


def merge_sort_r(lst, l1, m, r):
    """Recursive merge."""
    lst1 = lst[l1:m]
    lst2 = lst[m:r]
    if len(lst1) > 1:
        lst1 = merge_sort(lst1)
    if len(lst2) > 1:
        lst2 = merge_sort(lst2)
    return merge_list(lst1, lst2)


def merge_list(lst1, lst2):
    """Merge the already sorted lists."""
    res = []
    i = 0
    j = 0
    while i != len(lst1) and j != len(lst2):
        if lst1[i] < lst2[j]:
            res.append(lst1[i])
            i = i+1
        else:
            res.append(lst2[j])
            j = j+1
    res.extend(lst1[i:])
    res.extend(lst2[j:])
    return res


# %%


def test_mergesort1():
    """Test case for merge sort."""
    assert merge_sort([10, 2, 3, 5, 6, 8, 7]) == [2, 3, 5, 6, 7, 8, 10]


def test_mergesort2():
    """Test case with already sorted list."""
    assert merge_sort([2, 3, 5, 6, 8, 11]) == [2, 3, 5, 6, 8, 11]


def test_mergesort3():
    """Test case with repeated elements."""
    assert merge_sort([2, 3, 5, 3, 8, 2]) == [2, 2, 3, 3, 5, 8]


def test_mergesort4():
    """Test case with one element."""
    assert merge_sort([68]) == [68]


def test_mergesort5():
    """Test case with two elements."""
    assert merge_sort([21, 19]) == [19, 21]


def test_mergesort6():
    """Test case with empty ist."""
    assert merge_sort([]) is None


def test_mergesort7():
    """Test case with negative numbers."""
    assert merge_sort([-1, 2, 6, 3, 8, -9, -12]) == [-12, -9, -1, 2, 3, 6, 8]


def test_mergesort8():
    """Test case with floats."""
    assert (merge_sort([1.3, 1.4, -2.8, 32, 32.0, 68]) ==
            [-2.8, 1.3, 1.4, 32.0, 32, 68])
